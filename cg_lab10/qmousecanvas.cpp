#include "qmousecanvas.h"

QMouseCanvas::QMouseCanvas(QWidget *parent) :
    QWidget(parent), _image(new QImage(this->size(), QImage::Format_RGB32)),
    _painter(new QPainter()), _color(Qt::white) {
    this->setAutoFillBackground(false);
    fill();
}

QMouseCanvas::~QMouseCanvas() {
    delete _painter;
    delete _image;
}

void QMouseCanvas::mouseMoveEvent(QMouseEvent *event) {
    emit mouseMoved(event);
}

void QMouseCanvas::mousePressEvent(QMouseEvent *event) {
    emit mousePressed(event);
}

void QMouseCanvas::mouseReleaseEvent(QMouseEvent *event) {
    emit mouseReleased(event);
}

void QMouseCanvas::mouseDoubleClickEvent(QMouseEvent *event) {
    emit mouseDoubleClicked(event);
}

void QMouseCanvas::paintEvent(QPaintEvent *event) {
    _painter->begin(this);
    _painter->setClipRegion(event->region());
    _painter->drawImage(event->rect().topLeft(), *_image, event->rect());
    _painter->end();
}

void QMouseCanvas::resizeEvent(QResizeEvent *event) {
    QImage* _nimage = new QImage(event->size(), QImage::Format_RGB32);
    _nimage->fill(_color);
    _painter->begin(_nimage);
    _painter->drawImage(0, 0, *_image);
    _painter->end();
    delete _image;
    _image = _nimage;
}

QColor QMouseCanvas::fillColor() const {
    return _color;
}

void QMouseCanvas::setFillColor(const QColor& _ncolor) {
    _color = _ncolor;
}

QImage *QMouseCanvas::image() {
    return _image;
}

void QMouseCanvas::fill() {
    _image->fill(_color);
}

void QMouseCanvas::setImage(QImage *image) {
    fill();
    _painter->begin(_image);
    _painter->drawImage(0, 0, *image);
    _painter->end();
    repaint();
}
