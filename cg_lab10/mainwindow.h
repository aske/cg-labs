#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMouseEvent>
#include <QImage>
#include <QPainter>
#include <QLine>
#include <QPoint>
#include <QVector>
#include <QRect>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private:
    Ui::MainWindow *ui;
    QColor _polygonColor, _visibleColor, _clipperColor;
    bool _drawingPolygon;
    bool _drawingClipper;
    QLine _tempPolygonEdge;
    QLine _tempClipperEdge;
    QVector<QLine> _polygon;
    QVector<QLine> _clipper;
    void updatePolygonColor();
    void updateVisibleColor();
    void updateClipperColor();
    void clearCanvas(bool repaint = true);
    void drawPolygon(bool repaint = true);
    void drawClipper(bool repaint = true);
    void drawVisiblePolygon(const QVector<QPoint> &visiblePolygon);

public slots:
    void on_mouseCanvas_mouseMoved(QMouseEvent* event);
    void on_mouseCanvas_mousePressed(QMouseEvent *event);
    void on_pushButton_polygonColor_clicked();
    void on_pushButton_visibleColor_clicked();
    void on_pushButton_clipperColor_clicked();
    void on_pushButton_clear_clicked();
    void on_pushButton_clip_clicked();
};

#endif // MAINWINDOW_H
