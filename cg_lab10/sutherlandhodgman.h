#ifndef SUTHERLANDHODGMAN_H
#define SUTHERLANDHODGMAN_H

#include <QVector>
#include <QPoint>
#include <QLine>

QVector<QPoint> SutherlandHodgman(const QVector<QLine>& polygon,
                                  const QVector<QLine>& clipper);

#endif // SUTHERLANDHODGMAN_H
