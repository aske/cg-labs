#include <qmath.h>
#include <QColorDialog>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "sutherlandhodgman.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    _polygonColor(Qt::black),
    _clipperColor(Qt::green),
    _visibleColor(Qt::red),
    _drawingPolygon(false),
    _drawingClipper(false) {
    ui->setupUi(this);
    updatePolygonColor();
    updateVisibleColor();
    updateClipperColor();
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::on_pushButton_polygonColor_clicked() {
    _polygonColor = QColorDialog::getColor(_polygonColor, this);
    updatePolygonColor();
}

void MainWindow::on_pushButton_clipperColor_clicked() {
    _clipperColor = QColorDialog::getColor(_clipperColor, this);
    updateClipperColor();
}

void MainWindow::on_pushButton_visibleColor_clicked() {
    _visibleColor = QColorDialog::getColor(_visibleColor, this);
    updateVisibleColor();
}

void MainWindow::on_pushButton_clear_clicked() {
    _polygon.clear();
    _clipper.clear();
    clearCanvas();
}

void MainWindow::clearCanvas(bool repaint) {
    ui->mouseCanvas->setFillColor(Qt::white);
    ui->mouseCanvas->fill();
    if (repaint)
        ui->mouseCanvas->repaint();
}

void MainWindow::drawVisiblePolygon(const QVector<QPoint>& visiblePolygon) {
    QImage* image = ui->mouseCanvas->image();
    QPainter* painter = new QPainter(image);
    painter->setPen(QPen(_visibleColor, 3));
    painter->drawPolygon(visiblePolygon);
    delete painter;
    ui->mouseCanvas->repaint();
}

void MainWindow::on_pushButton_clip_clicked() {
    QVector<QPoint> visiblePolygon = SutherlandHodgman(_polygon, _clipper);
    drawVisiblePolygon(visiblePolygon);
}

void MainWindow::drawPolygon(bool repaint) {
    QImage* image = ui->mouseCanvas->image();
    QPainter* painter = new QPainter(image);
    painter->setPen(_polygonColor);
    painter->drawLines(_polygon);
    delete painter;
    if (repaint)
        ui->mouseCanvas->repaint();
}

void MainWindow::drawClipper(bool repaint) {
    QImage* image = ui->mouseCanvas->image();
    QPainter* painter = new QPainter(image);
    painter->setPen(QPen(_clipperColor, 1));
    painter->drawLines(_clipper);
    delete painter;
    if (repaint)
        ui->mouseCanvas->repaint();
}

void MainWindow::on_mouseCanvas_mouseMoved(QMouseEvent* event) {
    ui->labelCoords->setText(QString("X: %1, Y: %2")
                             .arg(QString::number(event->x()))
                             .arg(QString::number(event->y())));
    if (_drawingPolygon) {
        clearCanvas(false);
        drawPolygon(false);
        QImage* image = ui->mouseCanvas->image();
        QPainter* painter = new QPainter(image);
        painter->setPen(_polygonColor);
        _tempPolygonEdge.setP2(QPoint(event->x(), event->y()));
        painter->drawLine(_tempPolygonEdge);
        delete painter;
        drawPolygon(false);
        drawClipper(false);
    } else if (_drawingClipper) {
        clearCanvas(false);
        QImage* image = ui->mouseCanvas->image();
        QPainter* painter = new QPainter(image);
        painter->setPen(_clipperColor);
        _tempClipperEdge.setP2(QPoint(event->x(), event->y()));
        painter->drawLine(_tempClipperEdge);
        delete painter;
        drawPolygon(false);
        drawClipper(false);
    }
    ui->mouseCanvas->repaint();
}

void MainWindow::on_mouseCanvas_mousePressed(QMouseEvent *event) {
    bool leftMouseButton = event->button() == Qt::LeftButton;
    if (!_drawingPolygon && !_drawingClipper) {
        if (leftMouseButton) {
            _tempPolygonEdge.setP1(event->pos());
            _drawingPolygon = true;
        } else {
            _tempClipperEdge.setP1(event->pos());
            _drawingClipper = true;
        }
    }
    else if (_drawingPolygon) {
        if (leftMouseButton) {
            //_tempPolygonEdge.setP2(event->pos());
           // _polygon.push_back(_tempPolygonEdge);

            bool leftMouseButton = event->button() == Qt::LeftButton;
            bool shiftModifier = event->modifiers() == Qt::ShiftModifier;
            bool closeMouseButton = leftMouseButton && shiftModifier;

            if (!closeMouseButton && leftMouseButton) {
                _tempPolygonEdge.setP2(event->pos());
                _polygon.push_back(_tempPolygonEdge);
                _tempPolygonEdge.setP1(event->pos());
            } else if (closeMouseButton) {
                if (_polygon.size() >= 2) {
                    _tempPolygonEdge.setP2(_polygon.at(0).p1());
                    _polygon.push_back(_tempPolygonEdge);
                    _drawingPolygon = false;
                    clearCanvas(false);
                    drawClipper(false);
                    drawPolygon();
                }
            } else {
                _drawingPolygon = false;
                clearCanvas(false);
                drawClipper(false);
                drawPolygon();
            }
        } else {
            clearCanvas();
            drawPolygon();
            drawClipper();
        }
        //_drawingPolygon = false;
    } else if (_drawingClipper) {
        bool rightMouseButton = event->button() == Qt::RightButton;
        bool shiftModifier = event->modifiers() == Qt::ShiftModifier;
        bool closeMouseButton = rightMouseButton && shiftModifier;

        if (!closeMouseButton && rightMouseButton) {
            _tempClipperEdge.setP2(event->pos());
            _clipper.push_back(_tempClipperEdge);
            _tempClipperEdge.setP1(event->pos());
        } else if (closeMouseButton) {
            if (_clipper.size() >= 2) {
                _tempClipperEdge.setP2(_clipper.at(0).p1());
                _clipper.push_back(_tempClipperEdge);
                _drawingClipper = false;
                clearCanvas(false);
                drawClipper(false);
                drawPolygon();
            }
        } else {
            _drawingClipper = false;
            clearCanvas(false);
            drawClipper(false);
            drawPolygon();
        }
    }
}

void MainWindow::updatePolygonColor() {
    ui->pushButton_polygonColor->setStyleSheet(QString("* { background-color: rgb(%1, %2, %3) }")
                                            .arg(_polygonColor.red()).arg(_polygonColor.green())
                                            .arg(_polygonColor.blue()));
    drawPolygon();
}

void MainWindow::updateVisibleColor() {
    ui->pushButton_visibleColor->setStyleSheet(QString("* { background-color: rgb(%1, %2, %3) }")
                                               .arg(_visibleColor.red()).arg(_visibleColor.green())
                                               .arg(_visibleColor.blue()));
    drawPolygon();
}

void MainWindow::updateClipperColor() {
    ui->pushButton_clipperColor->setStyleSheet(QString("* { background-color: rgb(%1, %2, %3) }")
                                               .arg(_clipperColor.red()).arg(_clipperColor.green())
                                               .arg(_clipperColor.blue()));
    drawClipper();
}
