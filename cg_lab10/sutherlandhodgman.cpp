#include "sutherlandhodgman.h"

#include <QVector2D>

typedef QVector<QVector2D> Normals;

template <typename T> inline int sign(T value) {
    return (T(0) < value) - (value < T(0));
}

bool GetDirection(QPoint v1, QPoint v2, QPoint v3) {
    return ((v2.x()-v1.x())*(v3.y()-v2.y())
            - (v3.x()-v2.x())*(v2.y()-v1.y()) < 0);
}

bool GetDirection(QPoint v1, QPoint v2, QPoint v3, QPoint v4) {
    return ((v2.x()-v1.x())*(v4.y()-v3.y())
            - (v4.x()-v3.x())*(v2.y()-v1.y()) < 0);
}


bool GetPolygonDirection(const QVector<QPoint>& polygon) {
    QPoint p0(0, 0);
    double sum = 0;
    for (int i = 0; i < polygon.size()-1; ++i) {
        sum += (polygon[i].x()-p0.x())*(polygon[i+1].y()-polygon[i].y())
                - (polygon[i+1].x()-polygon[i].x())*(polygon[i].y()-p0.y());
    }
    return (sum < 0);
}

bool DoesIntersect(QPoint c0, QPoint c1, QPoint s, QPoint p) {
    return (GetDirection(c0, c1, s) ^ GetDirection(c0, c1, p));
}

QPoint GetIntersection(QPoint c0, QPoint c1, QPoint s, QPoint p) {
    double dsy = p.y()-s.y();
    double dsx = p.x()-s.x();
    double dcx = c1.x()-c0.x();
    double dcy = c1.y()-c0.y();

    double m = ((s.x()-c0.x())*dsy - (s.y()-c0.y())*dsx)/(dcx*dsy - dsx*dcy);

    return QPoint(c0.x() + (m*dcx), c0.y() + (m*dcy));
}

QVector<QPoint> SutherlandHodgmanClip(QVector<QPoint> polygon,
                                      const QVector<QPoint>& clipper,
                                      bool direction) {

    for (int i = 0; i < clipper.size()-1; ++i) {
        QVector<QPoint> visiblePolygon;
        QPoint start = polygon.at(0), end = polygon.at(0);

        for (int j = 0; j < polygon.size(); ++j) {
            if (DoesIntersect(clipper.at(i), clipper.at(i+1), start, polygon.at(j))) {
                visiblePolygon.push_back(GetIntersection(clipper.at(i), clipper.at(i+1),
                                            start, polygon.at(j)));
            }
            start = polygon.at(j);
            if (GetDirection(clipper.at(i), clipper.at(i+1), clipper.at(i), start) == direction) {
                visiblePolygon.push_back(start);
            }
        }
        if (visiblePolygon.size() != 0) {
            if (DoesIntersect(clipper.at(i), clipper.at(i+1), start, end)) {
                visiblePolygon.push_back(GetIntersection(clipper.at(i), clipper.at(i+1),
                                            start, end));
            }
        }
        polygon = visiblePolygon;
    }

    if (polygon.size() != 0) {
        polygon.push_back(polygon.at(0));
    }
    return polygon;
}

Normals computeNormals(const QVector<QPoint>& clipper) {
    Normals normals;

    double prevMul = 0;
    QVector2D prevVect(clipper.at(1).x()-clipper.at(0).x(),
                       clipper.at(1).y()-clipper.at(0).y());

    for (int i = 2; i < clipper.size(); ++i) {
        QVector2D nextVect(clipper.at(i).x()-clipper.at(i-1).x(),
                           clipper.at(i).y()-clipper.at(i-1).y());

        double newMul = prevVect.x()*nextVect.y() - prevVect.y()*nextVect.x();
        if (!qFuzzyIsNull(prevMul)) {
            if (!qFuzzyIsNull(newMul)) {
                if (sign(newMul) != sign(prevMul))
                    return normals;
            }
        } else {
            prevMul = newMul;
        }
        prevVect = nextVect;
    }
    if (qFuzzyIsNull(prevMul))
        return normals;

    bool RightRotate = prevMul > 0;
    for (int i = 1; i < clipper.size(); ++i) {
        QVector2D Vect(clipper.at(i).x()-clipper.at(i-1).x(),
                       clipper.at(i).y()-clipper.at(i-1).y());

        if (!RightRotate)
            normals.push_back(QVector2D(Vect.y(), -Vect.x()));
        else
            normals.push_back(QVector2D(-Vect.y(), Vect.x()));
    }
    return normals;
}


QVector<QPoint> SutherlandHodgman(const QVector<QLine>& polygon,
                                  const QVector<QLine>& clipper) {

    QVector<QPoint> visiblePolygon;

    /* oh, my ... */
    QVector<QPoint> pointClipper;
    pointClipper.push_back(clipper.at(0).p1());
    for (int i = 0; i < clipper.size(); ++i) {
        pointClipper.push_back(clipper.at(i).p2());
    }

    QVector<QPoint> pointPolygon;
    pointPolygon.push_back(polygon.at(0).p1());
    for (int i = 0; i < polygon.size(); ++i) {
        pointPolygon.push_back(polygon.at(i).p2());
    }

    if (pointClipper.size() < 3 || pointPolygon.size() < 3)
        return visiblePolygon;

    Normals normals = computeNormals(pointClipper);
    if (normals.size() == 0)
        return visiblePolygon;

    visiblePolygon = SutherlandHodgmanClip(pointPolygon, pointClipper,
                                           GetPolygonDirection(pointClipper));
    return visiblePolygon;
}


