#-------------------------------------------------
#
# Project created by QtCreator 2012-05-25T12:26:00
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = cg_lab10
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    qmousecanvas.cpp \
    sutherlandhodgman.cpp

HEADERS  += mainwindow.h \
    qmousecanvas.h \
    sutherlandhodgman.h

FORMS    += mainwindow.ui
