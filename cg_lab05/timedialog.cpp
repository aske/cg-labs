#include "timedialog.h"
#include "ui_timedialog.h"
#include "drawing_algorithms.h"
#include <qwt/qwt_plot.h>
#include <qwt/qwt_plot_histogram.h>
#include <qwt/qwt_series_data.h>
#include <qwt/qwt_interval.h>
#include <qwt/qwt_legend.h>
#include <qwt/qwt_plot_grid.h>
#include <QVector>
#include <QImage>
#include <qmath.h>

TimeDialog::TimeDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TimeDialog) {
    ui->setupUi(this);

    ui->plot->setCanvasBackground(QBrush(Qt::white));
    ui->plot->setAxisTitle(QwtPlot::yLeft, QString::fromUtf8("Время"));
    ui->plot->setAxisTitle(QwtPlot::xBottom, QString::fromUtf8("Радиус"));

    QwtLegend* legend = new QwtLegend();
    ui->plot->insertLegend(legend);

    QwtPlotGrid* grid = new QwtPlotGrid();
    grid->enableX(false);
    grid->enableYMin(true);
    grid->setMajPen(QPen(Qt::white, 0, Qt::DotLine));
    grid->setMinPen(QPen(Qt::gray, 0, Qt::DotLine));
    grid->attach(ui->plot);

    const int kTestCount = 50;
    QColor color = Qt::black;
    QImage* image = new QImage(1000, 1000, QImage::Format_RGB32);
    const int axis_x = 0;
    const int axis_y = 0;
    const int step_x = 1;
    const int step_y = 1;
    const int quantity = 350;

    QwtPlotCurve* parametricCurve = new QwtPlotCurve("Параметрический");
    parametricCurve->setPen(QPen(Qt::green));
    parametricCurve->setSamples(TestVisualChars(Parametric, image, axis_x, axis_y,
                                                step_x, step_y, quantity, color, kTestCount));
    parametricCurve->attach(ui->plot);

    QwtPlotCurve* decartCurve = new QwtPlotCurve("Декартов");
    decartCurve->setPen(QPen(Qt::magenta));
    decartCurve->setSamples(TestVisualChars(Decart, image, axis_x, axis_y,
                                            step_x, step_y, quantity, color, kTestCount));
    decartCurve->attach(ui->plot);

    QwtPlotCurve* bresenhamCurve = new QwtPlotCurve("Брезенхем");
    bresenhamCurve->setPen(QPen(Qt::black));
    bresenhamCurve->setSamples(TestVisualChars(Bresenham, image, axis_x, axis_y,
                                              step_x, step_y, quantity, color, kTestCount));
    bresenhamCurve->attach(ui->plot);

    QwtPlotCurve* midpointCurve = new QwtPlotCurve("Средняя точка");
    midpointCurve->setPen(QPen(Qt::red));
    midpointCurve->setSamples(TestVisualChars(MiddlePoint, image, axis_x, axis_y,
                                              step_x, step_y, quantity, color, kTestCount));
    midpointCurve->attach(ui->plot);

    QwtPlotCurve* standardCurve = new QwtPlotCurve("Стандартный");
    standardCurve->setPen(QPen(Qt::blue));
    standardCurve->setSamples(TestVisualChars(StandardAlgorithm, image, axis_x, axis_y,
                                              step_x, step_y, quantity, color, kTestCount));
    standardCurve->attach(ui->plot);

    ui->plot->replot();
}

TimeDialog::~TimeDialog() {
    delete ui;
}
