#include <QElapsedTimer>
#include <QPainter>
#include <qmath.h>
#include "drawing_algorithms.h"

AlgorithmInfo StandardAlgorithm(const int& center_x, const int& center_y,
                                const int& axis_x, const int& axis_y,
                                QImage* image, const QColor& color) {
    QPainter painter(image);
    painter.setPen(color);
    QPoint center_point(center_x, center_y);
    QElapsedTimer timer;
    timer.start();
    painter.drawEllipse(center_point, axis_x, axis_y);
    quint64 elapsed = timer.nsecsElapsed();
    return elapsed;
}

inline void SetPixelMirrored(const int& center_x, const int& center_y,
                             const int& x, const int&y,
                             QImage* image, const uint& color) {
    image->setPixel(center_x - x, center_y - y, color);
    image->setPixel(center_x - x, center_y + y, color);
    image->setPixel(center_x + x, center_y - y, color);
    image->setPixel(center_x + x, center_y + y, color);
}

AlgorithmInfo Bresenham(const int& center_x, const int& center_y,
                        const int& axis_x, const int& axis_y,
                        QImage* image, const QColor& color) {
    uint rgb_color = color.rgb();
    QElapsedTimer timer;
    timer.start();

    if (axis_x == 0) {
        for (int i = -axis_y; i < axis_y; ++i)
            image->setPixel(center_x, center_y+i, rgb_color);
        quint64 elapsed = timer.nsecsElapsed();
        return elapsed;
    }
    if (axis_y == 0) {
        for (int i = -axis_x; i < axis_x; ++i)
            image->setPixel(center_x+i, center_y, rgb_color);
        quint64 elapsed = timer.nsecsElapsed();
        return elapsed;
    }

    int a_squared = axis_x*axis_x;
    int b_squared = axis_y*axis_y;

    int x = 0;
    int y = axis_y;

    int dpy = 4*axis_y*a_squared;
    int dpx = 2*b_squared;
    int delta = 2*b_squared + a_squared*(1-2*axis_y);
    int border = qRound(b_squared/qSqrt(a_squared + b_squared));

    while (y > border) {
        SetPixelMirrored(center_x, center_y, x, y, image, rgb_color);
        if (delta > 0) {
            --y;
            dpy -= 4*a_squared;
            delta -= dpy;
        }
        ++x;
        dpx += 4*b_squared;
        delta += dpx;
    }

    x = axis_x;
    y = 0;
    dpx = 4*b_squared*axis_x;
    dpy = 2*a_squared;
    delta = 2*a_squared + b_squared*(1-2*axis_x);
    border = qRound(a_squared/qSqrt(a_squared+b_squared));

    while (x > border) {
        SetPixelMirrored(center_x, center_y, x, y, image, rgb_color);
        if (delta > 0) {
            --x;
            dpx -= 4*b_squared;
            delta -= dpx;
        }
        ++y;
        dpy += 4*a_squared;
        delta += dpy;
    }
    quint64 elapsed = timer.nsecsElapsed();
    return elapsed;
}

AlgorithmInfo MiddlePoint(const int& center_x, const int& center_y,
                                const int& axis_x, const int& axis_y,
                                QImage* image, const QColor& color) {
    uint rgb_color = color.rgb();
    QElapsedTimer timer;
    timer.start();

    if (axis_x == 0) {
        for (int i = -axis_y; i < axis_y; ++i)
            image->setPixel(center_x, center_y+i, rgb_color);
        quint64 elapsed = timer.nsecsElapsed();
        return elapsed;
    }
    if (axis_y == 0) {
        for (int i = -axis_x; i < axis_x; ++i)
            image->setPixel(center_x+i, center_y, rgb_color);
        quint64 elapsed = timer.nsecsElapsed();
        return elapsed;
    }

    int b_squared = axis_y*axis_y;
    int a_squared = axis_x*axis_x;

    int y = axis_y;
    int x = 0;
    int test_function = b_squared + (axis_x * axis_y - axis_x / 2) *
            (axis_x * axis_y - axis_x / 2) - a_squared * b_squared;

    int border = qRound(b_squared / qSqrt(a_squared + b_squared));
    int delta_probe_x = b_squared;
    int delta_probe_y = -2*a_squared*axis_y;

    while (y >= border) {
        SetPixelMirrored(center_x, center_y, x, y, image, rgb_color);
        if (test_function > 0) {
            --y;
            delta_probe_y += 2*a_squared;
            test_function += delta_probe_y;
        }
        ++x;
        delta_probe_x += 2*b_squared;
        test_function += delta_probe_x;
    }

    y = 0;
    x = axis_x;
    test_function = a_squared + (axis_x*axis_y - axis_y / 2) *
            (axis_x*axis_y - axis_y / 2) - a_squared * b_squared;
    border = qRound(a_squared / qSqrt(a_squared + b_squared));
    delta_probe_x = -2 * b_squared * axis_x;
    delta_probe_y = a_squared;

    while (x >= border) {
        SetPixelMirrored(center_x, center_y, x, y, image, rgb_color);
        if (test_function > 0) {
            --x;
            delta_probe_x += 2*b_squared;
            test_function += delta_probe_x;
        }
        ++y;
        delta_probe_y += 2*a_squared;
        test_function += delta_probe_y;
    }

    quint64 elapsed = timer.nsecsElapsed();
    return elapsed;
}

AlgorithmInfo Decart(const int& center_x, const int& center_y,
                     const int& axis_x, const int& axis_y,
                     QImage* image, const QColor& color) {
    uint rgb_color = color.rgb();
    QElapsedTimer timer;
    timer.start();

    if (axis_y == 0) {
        for (int i = -axis_x; i != axis_x; ++i)
            image->setPixel(center_x + i, center_y, rgb_color);
        quint64 elapsed = timer.nsecsElapsed();
        return elapsed;
    }
    if (axis_x == 0) {
        for (int i = -axis_y; i != axis_y; ++i)
            image->setPixel(center_x, center_y + i, rgb_color);
        quint64 elapsed = timer.nsecsElapsed();
        return elapsed;
    }

    float border, a_squared, b_squared;
    int x, y;
    a_squared = axis_x*axis_x;
    b_squared = axis_y*axis_y;
    border = qSqrt(a_squared * a_squared / (b_squared + a_squared));
    x = 0;
    y = axis_y;
    SetPixelMirrored(center_x, center_y, x, y, image, rgb_color);
    while (x < border) {
        ++x;
        y = qRound(axis_y * qSqrt(1 - x*x / a_squared));
        SetPixelMirrored(center_x, center_y, x, y, image, rgb_color);
    }

    while (y >= 0) {
        --y;
        x = qRound(axis_x * qSqrt(1 - y*y / b_squared));
        SetPixelMirrored(center_x, center_y, x, y, image, rgb_color);
    }

    quint64 elapsed = timer.nsecsElapsed();
    return elapsed;
}

AlgorithmInfo Parametric(const int& center_x, const int& center_y,
                         const int& axis_x, const int& axis_y,
                         QImage* image, const QColor& color) {
    uint rgb_color = color.rgb();
    QElapsedTimer timer;
    timer.start();

    if (axis_y == 0) {
        for (int i = -axis_x; i != axis_x; ++i)
            image->setPixel(center_x + i, center_y, rgb_color);
        quint64 elapsed = timer.nsecsElapsed();
        return elapsed;
    }
    if (axis_x == 0) {
        for (int i = -axis_y; i != axis_y; ++i)
            image->setPixel(center_x, center_y + i, rgb_color);
        quint64 elapsed = timer.nsecsElapsed();
        return elapsed;
    }

    int x, y;
    float step = 1.0f / qMax(axis_x, axis_y);;
    for (float t = 0; t <= M_PI_2; t += step) {
        x = qRound(qCos(t) * axis_x);
        y = qRound(qSin(t) * axis_y);
        SetPixelMirrored(center_x, center_y, x, y, image, rgb_color);
    }

    quint64 elapsed = timer.nsecsElapsed();
    return elapsed;
}

QVector<QPointF> TestVisualChars(DrawingAlgorithm algorithm, QImage* image,
                                 const int& axis_x, const int& axis_y,
                                 const int& step_x, const int& step_y,
                                 const int& quantity, const QColor& color,
                                 const int& tests_quantity) {
    int center_x = image->width()/2;
    int center_y = image->height()/2;
    QVector<QPointF> time_values;

    float x = axis_x;
    float y = axis_y;

    for (int i = 0; i < quantity; ++i) {
        quint64 time = 0;
        for (int j = 0; j < tests_quantity; ++j)
            time += algorithm(center_x, center_y, x, y, image, color);
        time /= tests_quantity;
        time_values.push_back(QPointF(qMax(x, y), time));
        x += step_x;
        y += step_y;
    }
    return time_values;
}
