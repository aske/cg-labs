#ifndef DRAWING_ALGORITHMS_H
#define DRAWING_ALGORITHMS_H

#include <QImage>
#include <QVector>
#include <QPointF>

typedef quint64 AlgorithmInfo;
typedef AlgorithmInfo (*DrawingAlgorithm)(const int &, const int &,
                                          const int &, const int &,
                                          QImage *, const QColor&);

AlgorithmInfo StandardAlgorithm(const int& center_x, const int& center_y,
                                const int& axis_x, const int& axis_y,
                                QImage* image, const QColor& color = Qt::black);

AlgorithmInfo Bresenham(const int& center_x, const int& center_y,
                        const int& axis_x, const int& axis_y,
                        QImage* image, const QColor& color = Qt::black);

AlgorithmInfo MiddlePoint(const int& center_x, const int& center_y,
                          const int& axis_x, const int& axis_y,
                          QImage* image, const QColor& color = Qt::black);

AlgorithmInfo Decart(const int& center_x, const int& center_y,
                     const int& axis_x, const int& axis_y,
                     QImage* image, const QColor& color = Qt::black);

AlgorithmInfo Parametric(const int& center_x, const int& center_y,
                         const int& axis_x, const int& axis_y,
                         QImage* image, const QColor& color = Qt::black);

QVector<QPointF> TestVisualChars(DrawingAlgorithm algorithm, QImage* image,
                                 const int& axis_x, const int& axis_y,
                                 const int& step_x, const int& step_y,
                                 const int& quantity, const QColor& color, const int &tests_quantity);

#endif // DRAWING_ALGORITHMS_H
