#include <qmath.h>
#include <QColorDialog>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "drawing_algorithms.h"
#include "timedialog.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    _ellipseColor(Qt::white),
    _backgroundColor(Qt::black) {
    ui->setupUi(this);
    updateBackColor();
    updateForeColor();
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::on_pushButton_BackColor_clicked() {
    _backgroundColor = QColorDialog::getColor(_backgroundColor, this);
    updateBackColor();
}

void MainWindow::on_pushButton_EllipseColor_clicked() {
    _ellipseColor = QColorDialog::getColor(_ellipseColor, this);
    updateForeColor();
}

void MainWindow::on_pushButton_clear_clicked() {
    updateBackColor();
}

void MainWindow::on_pushButton_speed_clicked() {
    TimeDialog d;
    d.exec();
}


void MainWindow::on_mouseCanvas_mouseMoved(QMouseEvent* event) {
    ui->labelCoords->setText(QString("X: %1, Y: %2")
                             .arg(QString::number(event->x()))
                             .arg(QString::number(event->y())));
}

void MainWindow::updateForeColor() {
    ui->pushButton_EllipseColor->setStyleSheet(QString("* { background-color: rgb(%1, %2, %3) }")
                                            .arg(_ellipseColor.red()).arg(_ellipseColor.green())
                                            .arg(_ellipseColor.blue()));
}

void MainWindow::updateBackColor() {
    ui->pushButton_BackColor->setStyleSheet(QString("* { background-color: rgb(%1, %2, %3) }")
                                            .arg(_backgroundColor.red()).arg(_backgroundColor.green())
                                            .arg(_backgroundColor.blue()));
    ui->mouseCanvas->setFillColor(_backgroundColor);
    ui->mouseCanvas->fill();
    ui->mouseCanvas->repaint();
}

DrawingAlgorithm MainWindow::chooseAlgorithm() {
    DrawingAlgorithm algorithm;
    switch (ui->comboBoxMethod->currentIndex()) {
        case 0:
            algorithm = &Parametric;
            break;
        case 1:
            algorithm = Decart;
            break;
        case 2:
            algorithm = &Bresenham;
            break;
        case 3:
            algorithm = &MiddlePoint;
            break;
        case 4:
            algorithm = &StandardAlgorithm;
            break;
    }
    return algorithm;
}

void MainWindow::on_pushButton_drawEllipse_clicked() {  
    int center_x = ui->spinBox_centerX->value();
    int center_y = ui->spinBox_centerY->value();
    int axis_x = ui->spinBox_axisX->value();
    int axis_y = ui->spinBox_axisY->value();

    QImage* image = ui->mouseCanvas->image();
    DrawingAlgorithm algorithm = chooseAlgorithm();
    algorithm(center_x, center_y, axis_x, axis_y, image, _ellipseColor);
    ui->mouseCanvas->repaint();
}

void MainWindow::on_pushButton_drawVis_clicked() {
    int axis_x = ui->spinBox_testAxisX->value();
    int axis_y = ui->spinBox_testAxisY->value();
    int step_x = ui->spinBox_stepX->value();
    int step_y = ui->spinBox_stepY->value();
    int quantity = ui->spinBox_quantity->value();
    const int kTestQuantity = 1;

    QImage* image = ui->mouseCanvas->image();
    DrawingAlgorithm algorithm = chooseAlgorithm();

    TestVisualChars(algorithm, image, axis_x, axis_y, step_x, step_y, quantity,
                    _ellipseColor, kTestQuantity);
    ui->mouseCanvas->repaint();
}
