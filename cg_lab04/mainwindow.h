#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMouseEvent>
#include "drawing_algorithms.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private:
    Ui::MainWindow *ui;
    QColor _backgroundColor, _lineColor;
    DrawingAlgorithm chooseAlgorithm();

public slots:
    void on_mouseCanvas_mouseMoved(QMouseEvent* event);
    void on_pushButton_drawVis_clicked();
    void on_pushButton_drawLine_clicked();
    void on_pushButton_BackColor_clicked();
    void on_pushButton_LineColor_clicked();
    void updateForeColor();
    void updateBackColor();
    void on_pushButton_clear_clicked();
    void on_pushButton_speed_clicked();
    void on_pushButton_aliasing_clicked();
};

#endif // MAINWINDOW_H
