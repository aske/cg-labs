#ifndef ALIASINGDIALOG_H
#define ALIASINGDIALOG_H

#include <QDialog>
#include <QVector>
#include <QPointF>
#include <qwt/qwt_plot_grid.h>
#include <qwt/qwt_plot_curve.h>

namespace Ui {
class AliasingDialog;
}

class AliasingDialog : public QDialog {
    Q_OBJECT
    
public:
    explicit AliasingDialog(QWidget *parent = 0);
    ~AliasingDialog();

    void setData(const QVector<QPointF>& values);
    
private:
    Ui::AliasingDialog *ui;
    QwtPlotGrid* grid;
    QwtPlotCurve* curve;
};

#endif // ALIASINGDIALOG_H
