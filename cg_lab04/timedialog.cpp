#include "timedialog.h"
#include "ui_timedialog.h"
#include "drawing_algorithms.h"
#include <qwt/qwt_plot.h>
#include <qwt/qwt_plot_histogram.h>
#include <qwt/qwt_series_data.h>
#include <qwt/qwt_interval.h>
#include <qwt/qwt_legend.h>
#include <qwt/qwt_plot_grid.h>
#include <QVector>
#include <QImage>
#include <qmath.h>
#include "multihistogram.h"

TimeDialog::TimeDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TimeDialog) {
    ui->setupUi(this);

    int testImageWidth = 500;
    int testImageHeight = 500;
    int testCenterX = 250;
    int testCenterY = 250;
    int testLength = 240;
    float testStep = 0.1;
    float testMaxAngle = 2*M_PI;

    QVector<quint64> timeData;
    quint64 time = 0;
    QImage* image = new QImage(testImageWidth, testImageHeight, QImage::Format_RGB32);

    time = testVisualChars(digitalDifferentialAnalyzer, image, testCenterX, testCenterY, testLength, testStep, testMaxAngle, Qt::white, Qt::black);
    timeData.push_back(time);
    time = testVisualChars(bresenhamReal, image, testCenterX, testCenterY, testLength, testStep, testMaxAngle, Qt::white, Qt::black);
    timeData.push_back(time);
    time = testVisualChars(bresenhamInteger, image, testCenterX, testCenterY, testLength, testStep, testMaxAngle, Qt::white, Qt::black);
    timeData.push_back(time);
    time = testVisualChars(bresenhamAntialiasing, image, testCenterX, testCenterY, testLength, testStep, testMaxAngle, Qt::white, Qt::black);
    timeData.push_back(time);
    time = testVisualChars(standardAlgorithm, image, testCenterX, testCenterY, testLength, testStep, testMaxAngle, Qt::white, Qt::black);
    timeData.push_back(time);

    const int kNumberOfTest = 100;

    for (int i = 0; i < kNumberOfTest; ++i) {
        time = testVisualChars(digitalDifferentialAnalyzer, image, testCenterX, testCenterY, testLength, testStep, testMaxAngle, Qt::white, Qt::black);
        timeData[0] += time;
        time = testVisualChars(bresenhamReal, image, testCenterX, testCenterY, testLength, testStep, testMaxAngle, Qt::white, Qt::black);
        timeData[1] += time;
        time = testVisualChars(bresenhamInteger, image, testCenterX, testCenterY, testLength, testStep, testMaxAngle, Qt::white, Qt::black);
        timeData[2] += time;
        time = testVisualChars(bresenhamAntialiasing, image, testCenterX, testCenterY, testLength, testStep, testMaxAngle, Qt::white, Qt::black);
        timeData[3] += time;
        time = testVisualChars(standardAlgorithm, image, testCenterX, testCenterY, testLength, testStep, testMaxAngle, Qt::white, Qt::black);
        timeData[4] += time;
    }

    for (int i = 0; i < 5; ++i) {
        timeData[i] /= (kNumberOfTest+1);
    }

    const double transCoeff = 1e6;
    MultiHistogram histogram;
    histogram.addColumn("ДЦА", ((double)timeData[0]/transCoeff), Qt::cyan);
    histogram.addColumn("Брезенхем (действ.)", ((double)timeData[1]/transCoeff), Qt::red);
    histogram.addColumn("Брезенхем (целый)", ((double)timeData[2]/transCoeff), Qt::green);
    histogram.addColumn("Брезенхем (интенс.)", ((double)timeData[3]/transCoeff), Qt::blue);
    histogram.addColumn("Стандартный", ((double)timeData[4]/transCoeff), Qt::magenta);


    QwtLegend* legend = new QwtLegend();

    ui->plot->insertLegend(legend);
    ui->plot->setAxisTitle(QwtPlot::yLeft, QString("Время теста (мс)"));
    histogram.attach(ui->plot);

    QwtPlotGrid* grid = new QwtPlotGrid();
    grid->enableX(false);
    grid->enableYMin(true);
    grid->setMajPen(QPen(Qt::black, 0, Qt::DotLine));
    grid->setMinPen(QPen(Qt::gray, 0, Qt::DotLine));
    grid->attach(ui->plot);

    ui->plot->replot();
}

TimeDialog::~TimeDialog() {
    delete ui;
}
