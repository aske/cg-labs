#ifndef DRAWING_ALGORITHMS_H
#define DRAWING_ALGORITHMS_H

#include <QImage>
#include <QPair>

typedef QPair<quint64, uint> AlgorithmInfo;
typedef AlgorithmInfo (*DrawingAlgorithm)(const int &, const int &, const int &, const int &, QImage *, const QColor&, const QColor&);

AlgorithmInfo digitalDifferentialAnalyzer(const int& x1, const int& y1, const int& x2, const int& y2, QImage* image, const QColor& color, const QColor& backgroundColor = Qt::white);
AlgorithmInfo bresenhamInteger(const int& x1, const int& y1, const int& x2, const int& y2, QImage* image, const QColor& color, const QColor& backgroundColor = Qt::white);
AlgorithmInfo bresenhamReal(const int& x1, const int& y1, const int& x2, const int& y2, QImage* image, const QColor& color, const QColor& backgroundColor = Qt::white);
AlgorithmInfo bresenhamAntialiasing(const int& x1, const int& y1, const int& x2, const int& y2, QImage* image, const QColor& color, const QColor& backgroundColor);
AlgorithmInfo standardAlgorithm(const int& x1, const int& y1, const int& x2, const int& y2, QImage* image, const QColor& color, const QColor& backgroundColor = Qt::white);

quint64 testVisualChars(DrawingAlgorithm algorithm, QImage* image, const int& centerX, const int& centerY, const float& length, const double &step, const double &maxAngle, const QColor &color, const QColor &backgroundColor = Qt::white);

#endif // DRAWING_ALGORITHMS_H
