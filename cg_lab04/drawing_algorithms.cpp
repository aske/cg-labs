#include <QElapsedTimer>
#include <QPainter>
#include <qmath.h>
#include "drawing_algorithms.h"

AlgorithmInfo digitalDifferentialAnalyzer(const int& x1, const int& y1, const int& x2, const int& y2, QImage* image, const QColor& color, const QColor& backgroundColor) {
    QElapsedTimer timer;
    timer.start();

    const uint lineColor = color.rgb();

    float dX = x2-x1;
    float dY = y2-y1;

    int length = qMax(qAbs(dX), qAbs(dY));

    float x = x1;
    float y = y1;

    image->setPixel(qRound(x), qRound(y), lineColor);

    if (length == 0) {
        uint steps = 0;
        quint64 elapsed = timer.nsecsElapsed();
        return QPair<quint64, uint>(elapsed, steps);
    }

    dX /= length;
    dY /= length;
    for (int i = 1; i < length; ++i) {
        x += dX;
        y += dY;
        image->setPixel(qRound(x), qRound(y), lineColor);
    }

    quint64 elapsed = timer.nsecsElapsed();
    uint steps = qMin(qAbs(y-y1), qAbs(x-x1));
    return QPair<quint64, uint>(elapsed, steps);
}

AlgorithmInfo bresenhamInteger(const int& x1, const int& y1, const int& x2, const int& y2, QImage* image, const QColor& color, const QColor& backgroundColor) {
    QElapsedTimer timer;
    timer.start();

    const uint lineColor = color.rgb();

    int dX = x2-x1;
    int dY = y2-y1;

    int sX = (dX > 0) ? 1 : -1;
    int sY = (dY > 0) ? 1 : -1;

    dX = qAbs(dX);
    dY = qAbs(dY);
    bool swappedXY = false;
    if (dX < dY) {
        swappedXY = true;
        qSwap(dX, dY);
    }

    int x = x1;
    int y = y1;

    if (dX == 0) {
        image->setPixel(x, y, lineColor);
        uint steps = 0;
        quint64 elapsed = timer.nsecsElapsed();
        return QPair<quint64, uint>(elapsed, steps);
    }

    int error = 2*dY - dX;
    const int doubleDX = 2*dX;
    const int doubleDY = 2*dY;

    for (int i = 0; i <= dX; ++i) {
        image->setPixel(x, y, lineColor);

        if (error >= 0) {
            (swappedXY) ? x += sX : y += sY;
            error -= doubleDX;
        }

        (swappedXY) ? y += sY : x += sX;
        error += doubleDY;
    }
    quint64 elapsed = timer.nsecsElapsed();
    uint steps = (swappedXY) ? qAbs(x-x1) : qAbs(y-y1);
    return QPair<quint64, uint>(elapsed, steps);
}

AlgorithmInfo bresenhamReal(const int& x1, const int& y1, const int& x2, const int& y2, QImage* image, const QColor& color, const QColor& backgroundColor) {
    QElapsedTimer timer;
    timer.start();

    const uint lineColor = color.rgb();

    int dX = x2-x1;
    int dY = y2-y1;

    int sX = (dX > 0) ? 1 : -1;
    int sY = (dY > 0) ? 1 : -1;

    dX = qAbs(dX);
    dY = qAbs(dY);
    bool swappedXY = false;
    if (dX < dY) {
        swappedXY = true;
        qSwap(dX, dY);
    }

    int x = x1;
    int y = y1;

    if (dX == 0) {
        image->setPixel(x, y, lineColor);
        uint steps = 0;
        quint64 elapsed = timer.nsecsElapsed();
        return QPair<quint64, uint>(elapsed, steps);
    }

    float m = (float)dY/(float)dX;
    float error = m-0.5f;

    for (int i = 0; i <= dX; ++i) {
        image->setPixel(x, y, lineColor);

        if (error >= 0) {
            (swappedXY) ? x += sX : y += sY;
            error -= 1.0f;
        }

        (swappedXY) ? y += sY : x += sX;
        error += m;
    }
    quint64 elapsed = timer.nsecsElapsed();
    uint steps = (swappedXY) ? qAbs(x-x1) : qAbs(y-y1);
    return QPair<quint64, uint>(elapsed, steps);
}

AlgorithmInfo bresenhamAntialiasing(const int& x1, const int& y1, const int& x2, const int& y2, QImage* image, const QColor& color, const QColor& backgroundColor) {
    QElapsedTimer timer;
    timer.start();

    const int intensityMax = 255;

    int x = x1;
    int y = y1;

    int dX = x2-x1;
    int dY = y2-y1;

    int sX = (dX > 0) ? 1 : -1;
    int sY = (dY > 0) ? 1 : -1;

    dX = qAbs(dX);
    dY = qAbs(dY);

    bool swappedXY = false;
    if (dX < dY) {
        swappedXY = true;
        qSwap(dX, dY);
    }

    if (dX == 0) {
        image->setPixel(x, y, color.rgb());
        uint steps = 0;
        quint64 elapsed = timer.nsecsElapsed();
        return QPair<quint64, uint>(elapsed, steps);
    }

    float m = (float)intensityMax * (float)dY / (float)dX;
    float error = ((float)intensityMax)/2.0f;
    float w = ((float)intensityMax)-m;

    const int redBackground = backgroundColor.red();
    const int greenBackground = backgroundColor.green();
    const int blueBackground = backgroundColor.blue();

    const int redDiffNormalized = (color.red() - backgroundColor.red())/intensityMax;
    const int greenDiffNormalized = (color.green() - backgroundColor.green())/intensityMax;
    const int blueDiffNormalized = (color.blue() - backgroundColor.blue())/intensityMax;

    for (int i = 0; i <= dX; ++i) {
        int red = redBackground + ((int)error)*redDiffNormalized;
        int green = greenBackground + ((int)error)*greenDiffNormalized;
        int blue = blueBackground + ((int)error)*blueDiffNormalized;
        image->setPixel(x, y, qRgb(red, green, blue));

        if (error >= w) {
            x += sX;
            y += sY;
            error -= w;
        } else {
            (swappedXY) ? y += sY : x += sX;
            error += m;
        }
    }

    quint64 elapsed = timer.nsecsElapsed();
    uint steps = (swappedXY) ? qAbs(x-x1) : qAbs(y-y1);
    return QPair<quint64, uint>(elapsed, steps);
}

AlgorithmInfo standardAlgorithm(const int& x1, const int& y1, const int& x2, const int& y2, QImage* image, const QColor& color, const QColor &backgroundColor) {
    QPainter* painter = new QPainter(image);
    painter->setPen(color);
    QElapsedTimer timer;
    timer.start();
    painter->drawLine(x1, y1, x2, y2);
    quint64 elapsed = timer.nsecsElapsed();
    uint steps = qMin(qAbs(y2-y1), qAbs(x2-x1));
    delete painter;
    return QPair<quint64, uint>(elapsed, steps);
}

quint64 testVisualChars(DrawingAlgorithm algorithm, QImage* image, const int& centerX, const int& centerY, const float& length, const double& step, const double &maxAngle, const QColor& color, const QColor &backgroundColor) {
    quint64 time = 0;
    for (double angle = 0; angle < maxAngle; angle += step) {
        float additionX = length*qCos(angle);
        float additionY = length*qSin(angle);
        int x1 = (int)(centerX-additionX);
        int y1 = (int)(centerY+additionY);
        int x2 = (int)(centerX+additionX);
        int y2 = (int)(centerY-additionY);

        time += (algorithm(x1, y1, x2, y2, image, color, backgroundColor)).first;
    }
    return time;
}


