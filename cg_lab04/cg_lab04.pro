#-------------------------------------------------
#
# Project created by QtCreator 2012-04-10T13:48:59
#
#-------------------------------------------------

QT       += core gui

TARGET = cg_lab04
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    qmousecanvas.cpp \
    drawing_algorithms.cpp \
    timedialog.cpp \
    multihistogram.cpp \
    aliasingdialog.cpp

HEADERS  += mainwindow.h \
    qmousecanvas.h \
    drawing_algorithms.h \
    timedialog.h \
    multihistogram.h \
    aliasingdialog.h

FORMS    += mainwindow.ui \
    timedialog.ui \
    aliasingdialog.ui

LIBS += -lqwt
