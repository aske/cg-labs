#ifndef TIMEDIALOG_H
#define TIMEDIALOG_H

#include <QDialog>
#include <QImage>
#include <QPainter>
#include <qwt/qwt_plot.h>

namespace Ui {
class TimeDialog;
}

class TimeDialog : public QDialog {
    Q_OBJECT
    
public:
    explicit TimeDialog(QWidget *parent = 0);
    ~TimeDialog();
    
private:
    Ui::TimeDialog *ui;
};

#endif // TIMEDIALOG_H
