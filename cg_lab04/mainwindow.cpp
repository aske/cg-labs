#include <qmath.h>
#include <QColorDialog>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "drawing_algorithms.h"
#include "timedialog.h"
#include "aliasingdialog.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    _lineColor(Qt::white),
    _backgroundColor(Qt::black) {
    ui->setupUi(this);
    updateBackColor();
    updateForeColor();
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::on_pushButton_BackColor_clicked() {
    _backgroundColor = QColorDialog::getColor(_backgroundColor, this);
    updateBackColor();
}

void MainWindow::on_pushButton_LineColor_clicked() {
    _lineColor = QColorDialog::getColor(_lineColor, this);
    updateForeColor();
}

void MainWindow::on_pushButton_clear_clicked() {
    updateBackColor();
}

void MainWindow::on_pushButton_speed_clicked() {
    TimeDialog d;
    d.exec();
}


void MainWindow::on_mouseCanvas_mouseMoved(QMouseEvent* event) {
    ui->labelCoords->setText(QString("X: %1, Y: %2")
                             .arg(QString::number(event->x()))
                             .arg(QString::number(event->y())));
}

void MainWindow::updateForeColor() {
    ui->pushButton_LineColor->setStyleSheet(QString("* { background-color: rgb(%1, %2, %3) }")
                                            .arg(_lineColor.red()).arg(_lineColor.green())
                                            .arg(_lineColor.blue()));
}

void MainWindow::updateBackColor() {
    ui->pushButton_BackColor->setStyleSheet(QString("* { background-color: rgb(%1, %2, %3) }")
                                            .arg(_backgroundColor.red()).arg(_backgroundColor.green())
                                            .arg(_backgroundColor.blue()));
    ui->mouseCanvas->setFillColor(_backgroundColor);
    ui->mouseCanvas->fill();
    ui->mouseCanvas->repaint();
}

DrawingAlgorithm MainWindow::chooseAlgorithm() {
    DrawingAlgorithm algorithm;
    switch (ui->comboBoxMethod->currentIndex()) {
        case 0:
            algorithm = &digitalDifferentialAnalyzer;
            break;
        case 1:
            algorithm = &bresenhamReal;
            break;
        case 2:
            algorithm = &bresenhamInteger;
            break;
        case 3:
            algorithm = &bresenhamAntialiasing;
            break;
        case 4:
            algorithm = &standardAlgorithm;
            break;
    }
    return algorithm;
}

void MainWindow::on_pushButton_drawLine_clicked() {
    int x1 = ui->spinBox_X1->value();
    int y1 = ui->spinBox_Y1->value();
    int x2 = ui->spinBox_X2->value();
    int y2 = ui->spinBox_Y2->value();

    QImage* image = ui->mouseCanvas->image();
    DrawingAlgorithm algorithm = chooseAlgorithm();
    algorithm(x1, y1, x2, y2, image, _lineColor, _backgroundColor);

    ui->mouseCanvas->repaint();
}

void MainWindow::on_pushButton_drawVis_clicked() {
    float centerX = ui->mouseCanvas->width()/2;
    float centerY = ui->mouseCanvas->height()/2;
    float length = 0.95 * qMin(centerX, centerY);

    double step = ui->doubleSpinBox_Step->value();
    const double maxAngle = M_PI;

    QImage* image = ui->mouseCanvas->image();
    DrawingAlgorithm algorithm = chooseAlgorithm();

    testVisualChars(algorithm, image, centerX, centerY, length, step, maxAngle, _lineColor, _backgroundColor);
    ui->mouseCanvas->repaint();
}

void MainWindow::on_pushButton_aliasing_clicked() {

    int method = ui->comboBoxMethod->currentIndex();
    DrawingAlgorithm algorithm = chooseAlgorithm();
    QVector<QPointF>* points = new QVector<QPointF>();

    int testImageWidth = 600;
    int testImageHeight = 600;
    int centerX = 300;
    int centerY = 300;
    int length = 280;
    float step = 0.01;
    float maxAngle = M_PI_2;

    QImage* image = new QImage(testImageWidth, testImageHeight, QImage::Format_RGB32);

    int i = 0;
    for (double angle = 0; angle < maxAngle; angle += step, ++i) {
        float additionX = length*qCos(angle);
        float additionY = length*qSin(angle);
        int x1 = (int)(centerX-additionX);
        int y1 = (int)(centerY+additionY);
        int x2 = (int)(centerX+additionX);
        int y2 = (int)(centerY-additionY);

        uint steps = (algorithm(x1, y1, x2, y2, image, _lineColor, _backgroundColor)).second;
        points->push_back(QPointF(angle*180/M_PI, steps));
    }
    AliasingDialog d;
    d.setData(*points);
    d.exec();
    delete points;
}
