#include "aliasingdialog.h"
#include "ui_aliasingdialog.h"

AliasingDialog::AliasingDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AliasingDialog) {
    ui->setupUi(this);

    ui->plot->setCanvasBackground(QBrush(Qt::white));
    ui->plot->setAxisTitle(QwtPlot::yLeft, QString::fromUtf8("Количество ступеней"));
    ui->plot->setAxisTitle(QwtPlot::xBottom, QString::fromUtf8("Угол"));
    grid = new QwtPlotGrid();
    grid->enableX(false);
    grid->enableYMin(true);
    grid->setMajPen(QPen(Qt::black, 0, Qt::DotLine));
    grid->setMinPen(QPen(Qt::gray, 0, Qt::DotLine));
    grid->attach(ui->plot);
    curve = new QwtPlotCurve();
    curve->attach(ui->plot);
}

AliasingDialog::~AliasingDialog() {
    delete grid;
    delete curve;
    delete ui;
}

void AliasingDialog::setData(const QVector<QPointF>& values) {
    curve->setSamples(values);
}
