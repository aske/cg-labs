#ifndef CONCAVETRIANGULATIONCLIP_H
#define CONCAVETRIANGULATIONCLIP_H

#include <QVector>
#include <QLine>

typedef QVector<QPoint> Triangle;

QVector<QLine> CyrusBeckConcaveLineClipper(QVector<QLine>& lines,
                                           QVector<QLine>& clipper,
                                           QVector<QVector<QPoint> > &triangles);

#endif // CONCAVETRIANGULATIONCLIP_H
