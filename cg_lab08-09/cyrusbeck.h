#ifndef CYRUSBECK_H
#define CYRUSBECK_H

#include <QVector>
#include <QVector2D>t
#include <QLine>

typedef QVector<QLine> Clipper;
typedef QVector<QVector2D> Normals;

inline int sign(double x) {
    return (x > 0) ? 1 : -1;
}

Normals computeNormals(const QVector<QPoint>& clipper);

QVector<QLine> CyrusBeckConvexClip(const QVector<QLine>& lines,
                                   const QVector<QPoint>& pointClipper,
                                   const Normals& normals);

QVector<QLine> CyrusBeckLinePointClipper(const QVector<QLine> &lines,
                                         const QVector<QPoint> &clipper);

QVector<QLine> CyrusBeckLineClipper(QVector<QLine>& lines,
                                    QVector<QLine>& clipper);

#endif // CYRUSBECK_H
