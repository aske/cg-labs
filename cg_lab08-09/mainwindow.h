#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMouseEvent>
#include <QImage>
#include <QPainter>
#include <QLine>
#include <QPoint>
#include <QVector>
#include <QRect>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private:
    Ui::MainWindow *ui;
    QColor _lineColor, _visibleColor, _clipperColor;
    bool _drawingLine;
    bool _drawingClipper;
    QLine _tempLine;
    QLine _tempClipperEdge;
    QVector<QLine> _lines;
    QVector<QLine> _clipper;
    void updateLineColor();
    void updateVisibleColor();
    void updateClipperColor();
    void clearCanvas(bool repaint = true);
    void drawLines(bool repaint = true);
    void drawClipper(bool repaint = true);
    void drawVisibleLines(const QVector<QLine>& visibleLines);
    void drawTriangles(const QVector<QVector<QPoint> >& triangles);

public slots:
    void on_mouseCanvas_mouseMoved(QMouseEvent* event);
    void on_mouseCanvas_mousePressed(QMouseEvent *event);
    void on_pushButton_lineColor_clicked();
    void on_pushButton_visibleColor_clicked();
    void on_pushButton_clipperColor_clicked();
    void on_pushButton_clear_clicked();
    void on_pushButton_clip_clicked();
    void on_pushButton_addLine_clicked();
};

#endif // MAINWINDOW_H
