#include "cyrusbeck.h"
#include <QVector2D>

Normals computeNormals(const QVector<QPoint>& clipper) {
    Normals normals;

    double prevMul = 0;
    QVector2D prevVect(clipper.at(1).x()-clipper.at(0).x(),
                       clipper.at(1).y()-clipper.at(0).y());

    for (int i = 2; i < clipper.size(); ++i) {
        QVector2D nextVect(clipper.at(i).x()-clipper.at(i-1).x(),
                           clipper.at(i).y()-clipper.at(i-1).y());

        double newMul = prevVect.x()*nextVect.y() - prevVect.y()*nextVect.x();
        if (!qFuzzyIsNull(prevMul)) {
            if (!qFuzzyIsNull(newMul)) {
                if (sign(newMul) != sign(prevMul))
                    return normals;
            }
        } else {
            prevMul = newMul;
        }
        prevVect = nextVect;
    }
    if (qFuzzyIsNull(prevMul))
        return normals;

    bool RightRotate = prevMul > 0;
    for (int i = 1; i < clipper.size(); ++i) {
        QVector2D Vect(clipper.at(i).x()-clipper.at(i-1).x(),
                       clipper.at(i).y()-clipper.at(i-1).y());

        if (!RightRotate)
            normals.push_back(QVector2D(Vect.y(), -Vect.x()));
        else
            normals.push_back(QVector2D(-Vect.y(), Vect.x()));
    }
    return normals;
}

bool clipLine(const QLine& line, const QVector<QPoint>& clipper,
              const Normals& normals, QLine& visibleLine)  {

    double tStart = 0;
    double tEnd = 1;

    QVector2D direction(line.p2().x()-line.p1().x(),
                        line.p2().y()-line.p1().y());

    for (int i = 0; i < clipper.size()-1; ++i) {
        QVector2D w(line.p1().x()-clipper.at(i).x(),
                    line.p1().y()-clipper.at(i).y());

        double normDirDot = QVector2D::dotProduct(direction, normals.at(i));
        double normWDot = QVector2D::dotProduct(w, normals.at(i));

        if (qFuzzyIsNull(normDirDot)) {
            if (normWDot < 0)
                return false;
        }

        double t = -normWDot/normDirDot;
        if (normDirDot > 0) {
            if (t > 1)
                return false;

            tStart = qMax(tStart, t);
            continue;
        }
        if (t < 0)
            return false;
        tEnd = qMin(tEnd, t);
    }

    if (tStart > tEnd)
        return false;

    QPoint newP1(line.p1().x() + qRound(direction.x()*tStart),
                 line.p1().y() + qRound(direction.y()*tStart));

    QPoint newP2(line.p1().x() + qRound(direction.x()*tEnd),
                 line.p1().y() + qRound(direction.y()*tEnd));


    visibleLine = QLine(newP1, newP2);
    return true;
}

QVector<QLine> CyrusBeckConvexClip(const QVector<QLine>& lines,
                                   const QVector<QPoint>& pointClipper,
                                   const Normals& normals) {
    QVector<QLine> visibleLines;

    foreach(QLine line, lines) {
        QLine visibleLine;
        if (clipLine(line, pointClipper, normals, visibleLine)) {
            visibleLines.push_back(visibleLine);
        }
    }
    return visibleLines;
}

QVector<QLine> CyrusBeckLinePointClipper(const QVector<QLine>& lines,
                                         const QVector<QPoint>& clipper) {
    QVector<QLine> visibleLines;

    if (clipper.size() < 3)
        return visibleLines;

    //Q_ASSERT(pointClipper.size() >= 3);

    Normals normals = computeNormals(clipper);

    //Q_ASSERT(normals.size() != 0);

    if (normals.size() == 0)
        return visibleLines;

    visibleLines = CyrusBeckConvexClip(lines, clipper, normals);

    return visibleLines;
}

QVector<QLine> CyrusBeckLineClipper(QVector<QLine>& lines,
                                    QVector<QLine>& clipper) {

    QVector<QLine> visibleLines;

    QVector<QPoint> pointClipper;
    pointClipper.push_back(clipper.at(0).p1());
    for (int i = 0; i < clipper.size(); ++i) {
        pointClipper.push_back(clipper.at(i).p2());
    }

    if (pointClipper.size() < 3)
        return visibleLines;

    //Q_ASSERT(pointClipper.size() >= 3);

    Normals normals = computeNormals(pointClipper);

    //Q_ASSERT(normals.size() != 0);

    if (normals.size() == 0)
        return visibleLines;

    visibleLines = CyrusBeckConvexClip(lines, pointClipper, normals);

    return visibleLines;
}


