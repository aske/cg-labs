#-------------------------------------------------
#
# Project created by QtCreator 2012-05-15T09:35:03
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = cg_lab08-09
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    qmousecanvas.cpp \
    cyrusbeck.cpp \
    concavetriangulationclip.cpp

HEADERS  += mainwindow.h \
    qmousecanvas.h \
    cyrusbeck.h \
    concavetriangulationclip.h

FORMS    += mainwindow.ui
