#include "concavetriangulationclip.h"
#include "cyrusbeck.h"

bool GetDirection(QPoint v1, QPoint v2, QPoint v3) {
    return ((v2.x()-v1.x())*(v3.y()-v2.y())
            - (v3.x()-v2.x())*(v2.y()-v1.y()) < 0);
}

bool GetClipperDirection(QVector<QPoint> clipper) {
    QPoint p0(0, 0);
    double areasSum = 0;
    for (int i = 0; i < clipper.size()-1; ++i) {
        areasSum += (clipper.at(i).x()-p0.x())*(clipper.at(i+1).y()-clipper.at(i).y())
                - (clipper.at(i+1).x()-clipper.at(i).x())*(clipper.at(i).y()-p0.y());
    }
    return (areasSum < 0);
}

QVector<Triangle> Triangulate(QVector<QPoint> clipper, bool mainDirection) {
    QVector<Triangle> triangles;
    if (clipper.size() > 3) {
        for (int i = 0; i < clipper.size()-2; ++i) {
            bool currentDirection = GetDirection(clipper.at(i),
                                                 clipper.at(i+1),
                                                 clipper.at(i+2));
            if (currentDirection == mainDirection) {
                bool pointInside = false;

                for (int j = 0; j < clipper.size()-1; ++j) {
                    if ((j < i) || (j > i+2)) {
                        bool dir1 = GetDirection(clipper.at(i), clipper.at(i+1),
                                                 clipper.at(j));
                        bool dir2 = GetDirection(clipper.at(i+1), clipper.at(i+2),
                                                 clipper.at(j));
                        bool dir3 = GetDirection(clipper.at(i+2), clipper.at(i),
                                                 clipper.at(j));
                        if ((dir1 && dir2 && dir3) == (dir1 || dir2 || dir3)) {
                            pointInside = true;
                            break;
                        }
                    }
                }

                if (!pointInside) {
                    Triangle triangle;
                    triangle.push_back(clipper.at(i));
                    triangle.push_back(clipper.at(i+1));
                    triangle.push_back(clipper.at(i+2));
                    triangle.push_back(clipper.at(i));
                    triangles.push_back(triangle);
                    clipper.remove(i+1);
                    i = 0;
                }
            }
        }

        Triangle triangle;
        triangle.push_back(clipper.at(0));
        triangle.push_back(clipper.at(1));
        triangle.push_back(clipper.at(2));
        triangle.push_back(clipper.at(0));
        triangles.push_back(triangle);
        if (clipper.size() > 4) {
            Triangle triangle;
            triangle.push_back(clipper.at(2));
            triangle.push_back(clipper.at(3));
            triangle.push_back(clipper.at(4));
            triangle.push_back(clipper.at(2));
        }
        return triangles;
    }
    return triangles;
}

QVector<QLine> CyrusBeckConcaveLineClipper(QVector<QLine>& lines,
                                           QVector<QLine>& clipper,
                                           QVector<QVector<QPoint> >& triangles) {
    QVector<QLine> visibleLines;

    QVector<QPoint> pointClipper;
    QVector<QPoint> clp;
    pointClipper.push_back(clipper.at(0).p1());
    for (int i = 0; i < clipper.size(); ++i) {
        pointClipper.push_back(clipper.at(i).p2());
    }

    if (pointClipper.size() < 3)
        return visibleLines;

    //Q_ASSERT(pointClipper.size() >= 3);

    Normals normals = computeNormals(pointClipper);

    //Q_ASSERT(normals.size() != 0);

    if (normals.size() == 0) {
        triangles = Triangulate(pointClipper, GetClipperDirection(pointClipper));
        foreach (Triangle trn, triangles)
            visibleLines += CyrusBeckLinePointClipper(lines, trn);
       // visibleLines += CyrusBeckLinePointClipper(lines, clp);
    }
    else
        visibleLines = CyrusBeckConvexClip(lines, pointClipper, normals);

    return visibleLines;
}
