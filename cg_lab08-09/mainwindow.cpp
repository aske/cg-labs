#include <qmath.h>
#include <QColorDialog>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "concavetriangulationclip.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    _lineColor(Qt::black),
    _clipperColor(Qt::green),
    _visibleColor(Qt::red),
    _drawingLine(false),
    _drawingClipper(false) {
    ui->setupUi(this);
    updateLineColor();
    updateVisibleColor();
    updateClipperColor();
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::on_pushButton_lineColor_clicked() {
    _lineColor = QColorDialog::getColor(_lineColor, this);
    updateLineColor();
}

void MainWindow::on_pushButton_clipperColor_clicked() {
    _clipperColor = QColorDialog::getColor(_clipperColor, this);
    updateClipperColor();
}

void MainWindow::on_pushButton_visibleColor_clicked() {
    _visibleColor = QColorDialog::getColor(_visibleColor, this);
    updateVisibleColor();
}

void MainWindow::on_pushButton_clear_clicked() {
    _lines.clear();
    _clipper.clear();
    clearCanvas();
}

void MainWindow::on_pushButton_addLine_clicked() {
    _lines.push_back(QLine(ui->spinBox_startX->value(), ui->spinBox_startY->value(),
                           ui->spinBox_endX->value(), ui->spinBox_endY->value()));
    clearCanvas(false);
    drawClipper(false);
    drawLines();
}

void MainWindow::clearCanvas(bool repaint) {
    ui->mouseCanvas->setFillColor(Qt::white);
    ui->mouseCanvas->fill();
    if (repaint)
        ui->mouseCanvas->repaint();
}

void MainWindow::drawVisibleLines(const QVector<QLine>& visibleLines) {
    QImage* image = ui->mouseCanvas->image();
    QPainter* painter = new QPainter(image);
    painter->setPen(QPen(_visibleColor, 3));
    foreach(QLine line, visibleLines)
        painter->drawLine(line);
    delete painter;
    ui->mouseCanvas->repaint();
}

void MainWindow::drawTriangles(const QVector<QVector<QPoint> >& triangles) {
    QImage* image = ui->mouseCanvas->image();
    QPainter* painter = new QPainter(image);
    painter->setPen(QPen(Qt::blue, 2, Qt::DotLine));
    foreach(QVector<QPoint> triangle, triangles) {
        painter->drawPolygon(triangle);
    }
    delete painter;
    ui->mouseCanvas->repaint();
}

void MainWindow::on_pushButton_clip_clicked() {
    QVector<QVector<QPoint> > triangles;
    QVector<QLine> visibleLines = CyrusBeckConcaveLineClipper(_lines, _clipper, triangles);
    drawVisibleLines(visibleLines);
    drawTriangles(triangles);
}

void MainWindow::drawLines(bool repaint) {
    QImage* image = ui->mouseCanvas->image();
    QPainter* painter = new QPainter(image);
    painter->setPen(_lineColor);
    painter->drawLines(_lines);
    delete painter;
    if (repaint)
        ui->mouseCanvas->repaint();
}

void MainWindow::drawClipper(bool repaint) {
    QImage* image = ui->mouseCanvas->image();
    QPainter* painter = new QPainter(image);
    painter->setPen(QPen(_clipperColor, 3));
    painter->drawLines(_clipper);
    delete painter;
    if (repaint)
        ui->mouseCanvas->repaint();
}

void MainWindow::on_mouseCanvas_mouseMoved(QMouseEvent* event) {
    ui->labelCoords->setText(QString("X: %1, Y: %2")
                             .arg(QString::number(event->x()))
                             .arg(QString::number(event->y())));
    if (_drawingLine) {
        clearCanvas(false);
        drawLines(false);
        QImage* image = ui->mouseCanvas->image();
        QPainter* painter = new QPainter(image);
        painter->setPen(_lineColor);
        _tempLine.setP2(QPoint(event->x(), event->y()));
        painter->drawLine(_tempLine);
        delete painter;
        drawLines(false);
        drawClipper(false);
    } else if (_drawingClipper) {
        clearCanvas(false);
        QImage* image = ui->mouseCanvas->image();
        QPainter* painter = new QPainter(image);
        painter->setPen(_clipperColor);
        _tempClipperEdge.setP2(QPoint(event->x(), event->y()));
        painter->drawLine(_tempClipperEdge);
        delete painter;
        drawLines(false);
        drawClipper(false);
    }
    ui->mouseCanvas->repaint();
}

void MainWindow::on_mouseCanvas_mousePressed(QMouseEvent *event) {
    bool leftMouseButton = event->button() == Qt::LeftButton;
    if (!_drawingLine && !_drawingClipper) {
        if (leftMouseButton) {
            _tempLine.setP1(event->pos());
            _drawingLine = true;
        } else {
            _tempClipperEdge.setP1(event->pos());
            _drawingClipper = true;
        }
    }
    else if (_drawingLine) {
        if (leftMouseButton) {
            _tempLine.setP2(event->pos());
            _lines.push_back(_tempLine);
        } else {
            clearCanvas();
            drawLines();
            drawClipper();
        }
        _drawingLine = false;
    } else if (_drawingClipper) {
        bool rightMouseButton = event->button() == Qt::RightButton;
        bool shiftModifier = event->modifiers() == Qt::ShiftModifier;
        bool closeMouseButton = rightMouseButton && shiftModifier;

        if (!closeMouseButton && rightMouseButton) {
            _tempClipperEdge.setP2(event->pos());
            _clipper.push_back(_tempClipperEdge);
            _tempClipperEdge.setP1(event->pos());
        } else if (closeMouseButton) {
            if (_clipper.size() >= 2) {
                _tempClipperEdge.setP2(_clipper.at(0).p1());
                _clipper.push_back(_tempClipperEdge);
                _drawingClipper = false;
                clearCanvas(false);
                drawClipper(false);
                drawLines();
            }
        } else {
            _drawingClipper = false;
            clearCanvas(false);
            drawClipper(false);
            drawLines();
        }
    }
}

void MainWindow::updateLineColor() {
    ui->pushButton_lineColor->setStyleSheet(QString("* { background-color: rgb(%1, %2, %3) }")
                                            .arg(_lineColor.red()).arg(_lineColor.green())
                                            .arg(_lineColor.blue()));
    drawLines();
}

void MainWindow::updateVisibleColor() {
    ui->pushButton_visibleColor->setStyleSheet(QString("* { background-color: rgb(%1, %2, %3) }")
                                               .arg(_visibleColor.red()).arg(_visibleColor.green())
                                               .arg(_visibleColor.blue()));
    drawLines();
}

void MainWindow::updateClipperColor() {
    ui->pushButton_clipperColor->setStyleSheet(QString("* { background-color: rgb(%1, %2, %3) }")
                                               .arg(_clipperColor.red()).arg(_clipperColor.green())
                                               .arg(_clipperColor.blue()));
    drawClipper();
}
