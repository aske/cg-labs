#include <qmath.h>
#include <QColorDialog>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "cohensutherland.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    _lineColor(Qt::black),
    _clipperColor(Qt::green),
    _visibleColor(Qt::red),
    _drawingLine(false),
    _drawingClipper(false) {
    ui->setupUi(this);
    updateLineColor();
    updateVisibleColor();
    updateClipperColor();
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::on_pushButton_lineColor_clicked() {
    _lineColor = QColorDialog::getColor(_lineColor, this);
    updateLineColor();
}

void MainWindow::on_pushButton_clipperColor_clicked() {
    _clipperColor = QColorDialog::getColor(_clipperColor, this);
    updateClipperColor();
}

void MainWindow::on_pushButton_visibleColor_clicked() {
    _visibleColor = QColorDialog::getColor(_visibleColor, this);
    updateVisibleColor();
}

void MainWindow::on_pushButton_clear_clicked() {
    _lines.clear();
    clearCanvas();
}

void MainWindow::on_pushButton_addLine_clicked() {
    _lines.push_back(QLine(ui->spinBox_startX->value(), ui->spinBox_startY->value(),
                           ui->spinBox_endX->value(), ui->spinBox_endY->value()));
    clearCanvas(false);
    drawClipper(false);
    drawLines();
}

void MainWindow::clearCanvas(bool repaint) {
    ui->mouseCanvas->setFillColor(Qt::white);
    ui->mouseCanvas->fill();
    if (repaint)
        ui->mouseCanvas->repaint();
}

void MainWindow::drawVisibleLines(const QVector<QLine>& visibleLines) {
    QImage* image = ui->mouseCanvas->image();
    QPainter* painter = new QPainter(image);
    painter->setPen(QPen(_visibleColor, 3));
    foreach(QLine line, visibleLines)
        painter->drawLine(line);
    delete painter;
    ui->mouseCanvas->repaint();
}

void MainWindow::on_pushButton_clip_clicked() {
    QVector<QLine> visibleLines = CohenSutherlandLineClipper(_lines, _clipper);
    drawVisibleLines(visibleLines);
}

void MainWindow::drawLines(bool repaint) {
    QImage* image = ui->mouseCanvas->image();
    QPainter* painter = new QPainter(image);
    painter->setPen(_lineColor);
    foreach(QLine line, _lines)
        painter->drawLine(line);
    delete painter;
    if (repaint)
        ui->mouseCanvas->repaint();
}

void MainWindow::drawClipper(bool repaint) {
    QImage* image = ui->mouseCanvas->image();
    QPainter* painter = new QPainter(image);
    painter->setPen(_clipperColor);
    painter->setBrush(QBrush(Qt::white, Qt::NoBrush));
    painter->drawRect(_clipper);
    delete painter;
    if (repaint)
        ui->mouseCanvas->repaint();
}

void MainWindow::on_mouseCanvas_mouseMoved(QMouseEvent* event) {
    ui->labelCoords->setText(QString("X: %1, Y: %2")
                             .arg(QString::number(event->x()))
                             .arg(QString::number(event->y())));
    if (_drawingLine) {
        clearCanvas(false);
        drawLines(false);
        QImage* image = ui->mouseCanvas->image();
        QPainter* painter = new QPainter(image);
        painter->setPen(_lineColor);
        _tempLine.setP2(QPoint(event->x(), event->y()));
        painter->drawLine(_tempLine);
        delete painter;
        drawLines(false);
        drawClipper(false);
    } else if (_drawingClipper) {
        clearCanvas(false);
        QImage* image = ui->mouseCanvas->image();
        QPainter* painter = new QPainter(image);
        painter->setPen(_clipperColor);
        _tempClipper.setBottomLeft(event->pos());
        painter->drawRect(_tempClipper);
        delete painter;
        drawLines(false);
    }
    ui->mouseCanvas->repaint();
}

void MainWindow::on_mouseCanvas_mousePressed(QMouseEvent *event) {
    bool leftMouseButton = event->button() == Qt::LeftButton;
    if (!_drawingLine && !_drawingClipper) {
        if (leftMouseButton) {
            _tempLine.setP1(event->pos());
            _drawingLine = true;
        } else {
            _tempClipper.setTopRight(event->pos());
            _drawingClipper = true;
        }
    }
    else if (_drawingLine) {
        if (leftMouseButton) {
            _tempLine.setP2(event->pos());
            _lines.push_back(_tempLine);
        } else {
            clearCanvas();
            drawLines();
            drawClipper();
        }
        _drawingLine = false;
    }
    else if (_drawingClipper) {
        if (!leftMouseButton) {
            _tempClipper.setBottomLeft(event->pos());
            _clipper = _tempClipper;
        } else {
            clearCanvas();
            drawLines();
        }
        _drawingClipper = false;
    }
}

void MainWindow::updateLineColor() {
    ui->pushButton_lineColor->setStyleSheet(QString("* { background-color: rgb(%1, %2, %3) }")
                                            .arg(_lineColor.red()).arg(_lineColor.green())
                                            .arg(_lineColor.blue()));
    drawLines();
}

void MainWindow::updateVisibleColor() {
    ui->pushButton_visibleColor->setStyleSheet(QString("* { background-color: rgb(%1, %2, %3) }")
                                               .arg(_visibleColor.red()).arg(_visibleColor.green())
                                               .arg(_visibleColor.blue()));
    drawLines();
}

void MainWindow::updateClipperColor() {
    ui->pushButton_clipperColor->setStyleSheet(QString("* { background-color: rgb(%1, %2, %3) }")
                                               .arg(_clipperColor.red()).arg(_clipperColor.green())
                                               .arg(_clipperColor.blue()));
    drawClipper();
}
