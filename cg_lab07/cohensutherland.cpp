#include "cohensutherland.h"

typedef int OutCode;

const OutCode kInside = 0;
const OutCode kLeft = 1;
const OutCode kRight = 2;
const OutCode kBottom = 4;
const OutCode kTop = 8;

inline OutCode ComputeOutCode(const int x, const int y, const QRect& clipper) {
    OutCode code = kInside;

    int x1 = qMin(clipper.left(), clipper.right());
    int x2 = qMax(clipper.left(), clipper.right());
    int y1 = qMin(clipper.top(), clipper.bottom());
    int y2 = qMax(clipper.top(), clipper.bottom());

    if (x < x1)
        code |= kLeft;
    else if (x > x2)
        code |= kRight;

    if (y < y1)
        code |= kTop;
    else if (y > y2)
        code |= kBottom;

    return code;
}

QVector<QLine> CohenSutherlandLineClipper(const QVector<QLine>& lines, const QRect& clipper) {
    QVector<QLine> visibleLines;
    foreach(QLine line, lines) {
        bool accept = false;
        OutCode codeP1 = ComputeOutCode(line.p1().x(), line.p1().y(), clipper);
        OutCode codeP2 = ComputeOutCode(line.p2().x(), line.p2().y(), clipper);

        int x1 = line.p1().x();
        int x2 = line.p2().x();
        int y1 = line.p1().y();
        int y2 = line.p2().y();

        while (true) {
            if (!(codeP1 | codeP2)) {
                accept = true;
                break;
            } else if (codeP1 & codeP2) {
                break;
            } else {
                /* Тут в общем все поправишь, норм зделаешь
                 * чтобы по 1му разу считалось
                 * Я как сделал на лабе быстренько
                 * таки и сдал
                 * ну ты понел
                 */
                int x, y;
                OutCode codeOutside = codeP1 ? codeP1 : codeP2;
                int top = qMax(clipper.top(), clipper.bottom());
                int bottom = qMin(clipper.top(), clipper.bottom());
                int right = qMax(clipper.right(), clipper.left());
                int left = qMin(clipper.right(), clipper.left());

                if (codeOutside & kTop) {
                    x = x1 + (x2 - x1)*(bottom - y1)/(y2 - y1);
                    y = bottom;
                } else if (codeOutside & kBottom) {
                    x = x1 + (x2 - x1)*(top - y1)/(y2 - y1);
                    y = top;
                } else if (codeOutside & kRight) {
                    x = right;
                    y = y1 + (y2 - y1)*(right - x1)/(x2 - x1);
                } else if (codeOutside & kLeft) {
                    x = left;
                    y = y1 + (y2 - y1)*(left - x1)/(x2 - x1);
                }

                if (codeOutside == codeP1) {
                    x1 = x;
                    y1 = y;
                    codeP1 = ComputeOutCode(x1, y1, clipper);
                } else {
                    x2 = x;
                    y2 = y;
                    codeP2 = ComputeOutCode(x2, y2, clipper);
                }
            }
        }

        if (accept)
            visibleLines.push_back(QLine(x1, y1, x2, y2));
    }

    return visibleLines;
}
