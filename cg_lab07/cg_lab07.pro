#-------------------------------------------------
#
# Project created by QtCreator 2012-05-11T21:55:13
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = cg_lab07
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    qmousecanvas.cpp \
    cohensutherland.cpp

HEADERS  += mainwindow.h \
    qmousecanvas.h \
    cohensutherland.h

FORMS    += mainwindow.ui
