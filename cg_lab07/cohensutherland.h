#ifndef COHENSUTHERLAND_H
#define COHENSUTHERLAND_H

#include <QVector>
#include <QLine>
#include <QRect>

QVector<QLine> CohenSutherlandLineClipper(const QVector<QLine>& lines, const QRect& clipper);

#endif // COHENSUTHERLAND_H
