#ifndef MULTIHISTOGRAM_H
#define MULTIHISTOGRAM_H

#include <QBrush>
#include <QVector>
#include <QColor>
#include <qwt/qwt.h>
#include <qwt/qwt_plot_histogram.h>
#include <qwt/qwt_interval.h>

class MultiHistogram {

public:
    MultiHistogram();
    ~MultiHistogram();

    void addColumn(const QString& title, const double value, const QColor& color);
    void attach(QwtPlot*& plot);

private:
    int _size;
    QVector<QwtPlotHistogram*> _histograms;

};

#endif // MULTIHISTOGRAM_H
