#include <QStack>
#include <QElapsedTimer>
#include <QPoint>
#include <QRgb>
#include <QSet>
#include <boost/foreach.hpp>
#include <algorithm>
#include "common.h"
#include "fillalgorithms.h"

#define foreach BOOST_FOREACH

qint64 FloodfillFill::fill(step_callback *callback) {
    int width = image->width(), height = image->height();
    QRgb rBorder(borderColor.rgb()), rFill(fillColor.rgb());
    QElapsedTimer timer;
    timer.start();

    QStack<QPoint> stack;
    stack.push(start);
    while (!stack.empty()) {
        QPoint curr = stack.top();
        stack.pop();
        int y = curr.y();
        bool done_smth = false;
        int left = curr.x(), right = left;
        for (; right < width; ++right) {
            QRgb curr = image->pixel(right, y);
            if (curr != rBorder && curr != rFill) {
                image->setPixel(right, y, rFill);
                done_smth = true;
            }
            else break;
        }
        --right;
        for (--left; left >= 0; --left) {
            QRgb curr = image->pixel(left, y);
            if (curr != rBorder && curr != rFill) {
                image->setPixel(left, y, rFill);
                done_smth = true;
            }
            else break;
        }
        ++left;
        if (y < height-1) {
            int cy = y+1;
            for (int cx = left; cx <= right; ++cx) {
                QRgb curr = image->pixel(cx, cy);
                if (curr != rBorder && curr != rFill) {
                        for (++cx; cx <= right; ++cx) {
                            if (image->pixel(cx, cy) == rBorder)
                                break;
                        }
                        stack.push(QPoint(cx, cy));
                }
            }
        }

        if (y > 0) {
            int cy = y-1;
            for (int cx = left; cx <= right; ++cx) {
                QRgb curr = image->pixel(cx, cy);
                if (curr != rBorder && curr != rFill) {
                        for (++cx; cx < right; ++cx) {
                            if (image->pixel(cx, cy) == rBorder)
                                break;
                        }
                        stack.push(QPoint(cx, cy));
                }
            }
        }

        if (done_smth && callback) {
            if (callback->step()) return timer.nsecsElapsed();
        }
    }
    return timer.nsecsElapsed();
}

struct arl_line {
    QLineF line;
    qreal a;
};

bool arl_less_than(const arl_line &a, const arl_line &b) {
    return a.line.p1().rx() < b.line.p1().rx();
}

qint64 LAEFill::fill(step_callback *callback) {
    int height = image->height();
    QRgb rFill(fillColor.rgb());
    QElapsedTimer timer;
    timer.start();
    QList<arl_line>** arl = new QList<arl_line>*[height];
    for (int i = 0; i < height; ++i) {
        arl[i] = NULL;
    }

    foreach (const QLine &line, *lines) {
        arl_line pushLine;
        pushLine.line = line;
        if (pushLine.line.dy() == 0) continue;
        if (pushLine.line.dy() < 0)
            pushLine.line.setPoints(pushLine.line.p2(), pushLine.line.p1());
        pushLine.a = pushLine.line.dx()/(float)pushLine.line.dy();
        int y = pushLine.line.p1().y();
        if (!arl[y])
            arl[y] = new QList<arl_line>();
        arl[y]->push_back(pushLine);
    }

    for (int i = 0; i < height; ++i) {
        if (!arl[i]) continue;
        qSort(arl[i]->begin(), arl[i]->end(), &arl_less_than);
        QList<arl_line>::const_iterator b = arl[i]->begin(), a = b++, end = arl[i]->end();
        for (; b < end; a += 2, b += 2) {
            int end = qRound(b->line.p1().x());
            int start = qRound(a->line.p1().x());
            if (end < start) {
                std::swap(start, end);
            }
            for (int j = start+1; j <= end-1; ++j) {
                image->setPixel(j, i, rFill);
            }
        }
        if (i < height-1) {
            foreach (const arl_line &line, *arl[i]) {
                arl_line nline;
                QPointF ns = line.line.p1();
                ++ns.ry();
                if (ns.ry() <= line.line.p2().ry()) {
                    ns.rx() += line.a;
                    nline.line = QLineF(ns, line.line.p2());
                    nline.a = line.a;
                    if (!arl[i+1])
                        arl[i+1] = new QList<arl_line>();
                    arl[i+1]->push_back(nline);
                }
            }
            delete arl[i];
        }

        if (callback) {
            if (callback->step()) return timer.nsecsElapsed();
        }
    }
    if (arl[height-1]) {
        delete arl[height-1];
    }
    delete[] arl;
    return timer.nsecsElapsed();
}

qint64 EdgesFill::fill(step_callback *callback) {
    int width = image->width();
    QRgb rFill(fillColor.rgb()), rBack(backColor.rgb());
    QElapsedTimer timer;
    timer.start();
    foreach (const QLine &line, *lines) {
        int dy = sgn(line.dy());
        if (dy == 0) continue;
        int fy = line.p2().y() + dy;
        qreal dx = line.dx()/(qreal)abs(line.dy());
        int y = line.p1().y();
        qreal x = line.p1().x();
        for (; y != fy; y += dy, x += dx) {
            for (int cx = qRound(x); cx < width; ++cx) {
                QRgb pixel = image->pixel(cx, y);
                if (pixel == rBack) {
                    image->setPixel(cx, y, rFill);
                } else {
                    image->setPixel(cx, y, rBack);
                }
            }
            if (callback) {
                if (callback->step()) return timer.nsecsElapsed();
            }
        }
    }
    return timer.nsecsElapsed();
}

void FlagFill::special_line(const QLine &line, const int width,
                            const QRgb &rBorder, const QRgb &rBack) {
    int dy = sgn(line.dy());
    if (dy == 0) return;
    int fy = line.p2().y() + dy;
    int y = line.p1().y();
    qreal x = line.p1().x();
    qreal dx = line.dx()/(qreal)abs(line.dy());
    for (; y != fy; y += dy, x += dx) {
        int cx = qRound(x);
        if (image->pixel(cx, y) != rBorder) {
            image->setPixel(cx, y, rBorder);
        } else if (cx < width-1) {
            ++cx;
            if (image->pixel(cx, y) != rBorder) {
                image->setPixel(cx, y, rBorder);
            } else {
                image->setPixel(cx, y, rBack);
            }
        }
    }
}

qint64 FlagFill::fill(step_callback *callback) {
    int height = image->height();
    int width = image->width();
    QRgb rBack(backColor.rgb()), rFill(fillColor.rgb()), rBorder(borderColor.rgb());
    image->fill(rBack);
    foreach (const QLine &line, *lines) {
        special_line(line, width, rBorder, rBack);
        if (callback) {
            if (callback->step()) return false;
        }
    }
    QElapsedTimer timer;
    timer.start();
    for (int y = 0; y < height; ++y) {
        bool flag = false;
        bool done_smth = false;
        for (int x = 0; x < width; ++x) {
            if (image->pixel(x, y) == rBorder) {
                flag = !flag;
                done_smth = true;
            }
            if (flag) {
                image->setPixel(x, y, rFill);
            }
        }
       if (callback && done_smth) {
            if (callback->step()) return false;
        }
    }
    return timer.nsecsElapsed();
}

qint64 BorderFill::fill(step_callback *callback) {
    int width = image->width();
    if (border > width) border = width;
    QRgb rFill(fillColor.rgb()), rBack(backColor.rgb());
    QElapsedTimer timer;
    timer.start();
    foreach (const QLine &line, *lines) {
        int dy = sgn(line.dy());
        if (dy == 0) continue;
        int fy = line.p2().y() + dy;
        qreal dx = line.dx() / (qreal)abs(line.dy());
        int y = line.p1().y();
        qreal x = line.p1().x();
        for (; y != fy; y += dy, x += dx) {
            for (int cx = qRound(x); cx < border; ++cx) {
                QRgb pixel = image->pixel(cx, y);
                if (pixel == rBack) {
                    image->setPixel(cx, y, rFill);
                } else {
                    image->setPixel(cx, y, rBack);
                }
            }

            if (callback) {
                if (callback->step()) return timer.nsecsElapsed();
            }
            for (int cx = qRound(x); cx >= border; --cx) {
                QRgb pixel = image->pixel(cx, y);
                if (pixel == rBack) {
                    image->setPixel(cx, y, rFill);
                } else {
                    image->setPixel(cx, y, rBack);
                }
            }
            if (callback) {
                if (callback->step()) return timer.nsecsElapsed();
            }
        }
    }
    return timer.nsecsElapsed();
}
