#include <QColorDialog>
#include <QElapsedTimer>
#include <cmath>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "common.h"
#include "fillalgorithms.h"
#include "timedialog.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    foreColor(Qt::black),
    backColor(Qt::white),
    fillColor(Qt::green),
    oldState(-1, -1),
    line_drawing(false),
    workerThread(NULL) {
    ui->setupUi(this);
    updateForeColor();
    updateBackColor();
    updateFillColor();
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::updateForeColor() {
    ui->pushButton_foreColor->setStyleSheet(QString("* { background-color: rgb(%1, %2, %3) }")
                                            .arg(foreColor.red()).arg(foreColor.green())
                                            .arg(foreColor.blue()));
}

void MainWindow::updateBackColor() {
    ui->pushButton_backColor->setStyleSheet(QString("* { background-color: rgb(%1, %2, %3) }")
                                            .arg(backColor.red()).arg(backColor.green())
                                            .arg(backColor.blue()));
    ui->canvas->setFillColor(backColor);
    ui->canvas->fill();
    ui->canvas->repaint();
}

void MainWindow::on_canvas_mouseMoved(QMouseEvent *event) {
    ui->label_coord->setText(QString("X: %1, Y: %2").arg(QString::number(event->x()), 5).arg(QString::number(event->y()), 5));
    if (workerThread) return;

     if ( line_drawing) {
        QLine line(oldState, event->pos());
        if (event->modifiers() & Qt::ShiftModifier) {
            if (abs(line.dx()) > abs(line.dy())) {
                line.setP2(QPoint(line.p2().x(), oldState.y()));
            } else {
                line.setP2(QPoint(oldState.x(), line.p2().y()));
            }
        }
        ui->canvas->setImage(lineBuffer);
        ui->canvas->painter()->drawLine(line);
        ui->canvas->repaint();
    }
}

void MainWindow::on_pushButton_foreColor_clicked() {
    foreColor = QColorDialog::getColor(foreColor, this);
    updateForeColor();
}

void MainWindow::on_pushButton_backColor_clicked() {
    backColor = QColorDialog::getColor(backColor, this);
    updateBackColor();
}

void MainWindow::on_pushButton_4_clicked() {
    if (workerThread) return;
    endLineDrawing();
    ui->listWidget->clear();
    lines.clear();
    ui->canvas->fill();
    ui->canvas->repaint();
}

void MainWindow::drawLines() {
    ui->canvas->fill();
    QPainter *painter = ui->canvas->painter();
    foreach (const QLine &line, lines) {
        painter->drawLine(line);
    }
    ui->canvas->repaint();
}

void MainWindow::on_canvas_mouseReleased(QMouseEvent *) {
    if (workerThread) return;
}

void MainWindow::on_canvas_mousePressed(QMouseEvent *event) {
    if (workerThread) return;

    if (event->buttons() & Qt::LeftButton) {
        line_drawing = true;
        if (oldState.x() != -1) {
            QLine line(oldState, event->pos());
            if (event->modifiers() & Qt::ShiftModifier) {
                if (abs(line.dx()) > abs(line.dy())) {
                    line.setP2(QPoint(line.p2().x(), oldState.y()));
                } else {
                    line.setP2(QPoint(oldState.x(), line.p2().y()));
                }
            }
            int dy = sgn(line.dy());
            QList<QLine>::const_iterator i = curr_lines->end();
            while (i != curr_lines->begin()) {
                --i;
                if (i->dy() == 0) continue;
                if (sgn(i->dy()) == dy) {
                    line.setP1(QPoint(line.p1().x(), line.p1().y() + dy));
                } else {
                    line.setP1(QPoint(line.p1().x()+1, line.p1().y()));
                }
                break;
            }
            curr_lines->push_back(line);
            ui->listWidget->addItem(QString("%1, %2; %3, %4").arg(line.x1())
                                    .arg(line.y1()).arg(line.x2())
                                    .arg(line.y2()));
            linePainter->drawLine(line);
            oldState = line.p2();
        } else {
            curr_lines = new QList<QLine>();
            lineBuffer = new QImage(*ui->canvas->image());
            linePainter = new QPainter(lineBuffer);
            oldState = event->pos();
        }
        return;

    } else if (event->buttons() & Qt::RightButton && line_drawing) {
        if (!curr_lines->empty()) {
            QLine line(oldState, curr_lines->first().p1());
            int dy = sgn(line.dy());
            QList<QLine>::const_iterator i = curr_lines->end();
            while (i != curr_lines->begin()) {
                --i;
                if (i->dy() == 0) continue;
                if (sgn(i->dy()) == dy) {
                    line.setP1(QPoint(line.p1().x(), line.p1().y() + dy));
                } else {
                    line.setP1(QPoint(line.p1().x() + 1, line.p1().y()));
                }
                break;
            }
            foreach (const QLine &curr, *curr_lines) {
                if (curr.dy() == 0) continue;
                if (sgn(curr.dy()) == dy) {
                    line.setP2(QPoint(line.p2().x(), line.p2().y() - dy));
                } else {
                    line.setP2(QPoint(line.p2().x() + 1, line.p2().y()));
                }
                break;
            }
            curr_lines->push_back(line);
            ui->listWidget->addItem(QString("%1, %2; %3, %4").arg(line.x1())
                                    .arg(line.y1()).arg(line.x2())
                                    .arg(line.y2()));
            linePainter->drawLine(line);
        }
        endLineDrawing();
        return;
    }

    if (event->buttons() & Qt::RightButton) {
        ui->spinBox_cx->setValue(event->pos().x());
        ui->spinBox_cy->setValue(event->pos().y());
    }
}

void MainWindow::endLineDrawing() {
    if (line_drawing) {
        line_drawing = false;
        oldState = QPoint(-1, -1);
        ui->canvas->setImage(lineBuffer);
        lines.append(*curr_lines);
        delete curr_lines;
        delete linePainter;
        delete lineBuffer;
    }
}

void MainWindow::on_pushButton_5_clicked() {
    if (workerThread) return;
    if (ui->listWidget->currentRow() >= 0) {
        endLineDrawing();
        lines.removeAt(ui->listWidget->currentRow());
        delete ui->listWidget->currentItem();
        drawLines();
    }
}

void MainWindow::on_pushButton_clicked() {
    if (workerThread) return;
    endLineDrawing();
    QLine line(ui->spinBox_ax->value(), ui->spinBox_ay->value(),
               ui->spinBox_bx->value(), ui->spinBox_by->value());
    lines.push_back(line);
    ui->listWidget->addItem(QString("%1, %2; %3, %4").arg(line.x1())
                            .arg(line.y1()).arg(line.x2())
                            .arg(line.y2()));
    ui->canvas->painter()->drawLine(line);
    ui->canvas->repaint();
}

class animation_callback : public step_callback {
public:
    worker_thread *worker;

    bool step() {
        worker->stepWait.unlock();
        worker->animationWait.lock();
        worker->stepWait.lock();
        worker->animationWait.unlock();
        return worker->stop;
    }
};

void worker_thread::run() {
    animation_callback callback;
    callback.worker = this;
    stepWait.lock();
    result = fill->fill(&callback);
    stepWait.unlock();
    emit completed();
}

const int timer_interval = 1000 / 60;

void MainWindow::on_pushButton_3_clicked() {
    if (workerThread) {
        animationTimer->stop();
        workerThread->stepWait.lock();
        if (workerThread->animation) {
            workerThread->animationWait.unlock();
        }
        workerThread->stop = true;
        workerThread->stepWait.unlock();
    } else {
        endLineDrawing();

        workerThread = new worker_thread();
        connect(workerThread, SIGNAL(completed()), this, SLOT(worker_finished()));
        switch (ui->comboBox->currentIndex()) {
        case 0: {
            FloodfillFill *fill = new FloodfillFill();
            fill->image = ui->canvas->image();
            fill->fillColor = fillColor;
            fill->borderColor = foreColor;
            fill->start = QPoint(ui->spinBox_cx->value(), ui->spinBox_cy->value());
            workerThread->fill = fill;
            break;
        }
        case 1: {
            EdgesFill *fill = new EdgesFill();
            fill->image = ui->canvas->image();
            fill->fillColor = fillColor;
            fill->backColor = backColor;
            fill->lines = &lines;
            workerThread->fill = fill;
            break;
        }
        case 2: {
            BorderFill *fill = new BorderFill();
            fill->image = ui->canvas->image();
            fill->fillColor = fillColor;
            fill->backColor = backColor;
            fill->lines = &lines;
            fill->border = ui->spinBox->value();
            workerThread->fill = fill;
            break;
        }
        case 3: {
            FlagFill *fill = new FlagFill();
            fill->image = ui->canvas->image();
            fill->fillColor = fillColor;
            fill->borderColor = foreColor;
            fill->backColor = backColor;
            fill->lines = &lines;
            workerThread->fill = fill;
            break;
        }
        case 4: {
            LAEFill *fill = new LAEFill();
            fill->image = ui->canvas->image();
            fill->fillColor = fillColor;
            fill->lines = &lines;
            workerThread->fill = fill;
            break;
        }
        }
        workerThread->animation = ui->checkBox->isChecked();
        if (workerThread->animation) {
            animationTimer = new QTimer(this);
            animationTimer->setInterval(timer_interval);
            animationTimer->setSingleShot(true);
            connect(animationTimer, SIGNAL(timeout()), this, SLOT(animation_tick()));
            workerThread->animationWait.lock();
            animationTimer->start();
        }
        workerThread->start();
        ui->pushButton_3->setText("Остановить");
    }
}

void MainWindow::worker_finished() {
    if (workerThread->animation) {
        animationTimer->stop();
        delete animationTimer;
    }
    workerThread->wait();
    delete workerThread;
    workerThread = NULL;
    ui->canvas->repaint();
    ui->pushButton_3->setText("Выполнить");
}

void MainWindow::animation_tick() {
    workerThread->stepWait.lock();
    workerThread->animationWait.unlock();
    ui->canvas->repaint();
    workerThread->stepWait.unlock();
    workerThread->animationWait.lock();
    animationTimer->start();
}

void MainWindow::updateFillColor() {
    ui->pushButton_6->setStyleSheet(QString("* { background-color: rgb(%1, %2, %3) }")
                                            .arg(fillColor.red()).arg(fillColor.green())
                                            .arg(fillColor.blue()));
}

void MainWindow::on_pushButton_6_clicked() {
    fillColor = QColorDialog::getColor(foreColor, this);
    updateFillColor();
}

void MainWindow::softClear() {
    if (workerThread) return;
    endLineDrawing();
    //ui->listWidget->clear();
    //lines.clear();
    ui->canvas->fill();
    drawLines();
    //ui->canvas->repaint();
}

void MainWindow::on_pushButton_softClear_clicked() {
    softClear();
}

void MainWindow::on_pushButton_speed_clicked() {

    QVector<quint64> timeData;
    quint64 time = 0;

    EdgesFill* edgeFill = new EdgesFill();
    edgeFill->image = ui->canvas->image();
    edgeFill->fillColor = fillColor;
    edgeFill->backColor = backColor;
    edgeFill->lines = &lines;

    BorderFill* borderFill = new BorderFill();
    borderFill->image = ui->canvas->image();
    borderFill->fillColor = fillColor;
    borderFill->backColor = backColor;
    borderFill->lines = &lines;
    borderFill->border = ui->spinBox->value();

    FlagFill* flagFill = new FlagFill();
    flagFill->image = ui->canvas->image();
    flagFill->fillColor = fillColor;
    flagFill->borderColor = foreColor;
    flagFill->backColor = backColor;
    flagFill->lines = &lines;

    LAEFill* laeFill = new LAEFill();
    laeFill->image = ui->canvas->image();
    laeFill->fillColor = fillColor;
    laeFill->lines = &lines;

    FloodfillFill* floodFill = new FloodfillFill();
    floodFill->image = ui->canvas->image();
    floodFill->fillColor = fillColor;
    floodFill->borderColor = foreColor;
    floodFill->start = QPoint(ui->spinBox_cx->value(), ui->spinBox_cy->value());

    /* Benchmark */
    time = floodFill->fill(NULL);
    timeData.push_back(time);
    softClear();
    time = edgeFill->fill(NULL);
    timeData.push_back(time);
    softClear();
    time = borderFill->fill(NULL);
    timeData.push_back(time);
    softClear();
    time = flagFill->fill(NULL);
    timeData.push_back(time);
    softClear();
    time = laeFill->fill(NULL);
    timeData.push_back(time);
    softClear();

    const int kNumberOfTest = 50;

    for (int i = 0; i < kNumberOfTest; ++i) {
        time = floodFill->fill(NULL);
        timeData[0] += time;
        softClear();
        time = edgeFill->fill(NULL);
        timeData[1] += time;
        softClear();
        time = borderFill->fill(NULL);
        timeData[2] += time;
        softClear();
        time = flagFill->fill(NULL);
        timeData[3] += time;
        softClear();
        time = laeFill->fill(NULL);
        timeData[4] += time;
        softClear();
    }

    for (int i = 0; i < 5; ++i) {
        timeData[i] /= (kNumberOfTest+1);
    }

    TimeDialog d;
    d.setTimeData(timeData);
    d.plot();
    d.exec();
}
