#include "timedialog.h"
#include "ui_timedialog.h"
#include "fillalgorithms.h"
#include <qwt/qwt_plot.h>
#include <qwt/qwt_plot_histogram.h>
#include <qwt/qwt_series_data.h>
#include <qwt/qwt_interval.h>
#include <qwt/qwt_legend.h>
#include <qwt/qwt_plot_grid.h>
#include <QVector>
#include <QImage>
#include <qmath.h>
#include "multihistogram.h"


TimeDialog::TimeDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TimeDialog) {
    ui->setupUi(this);
}

void TimeDialog::setTimeData(const QVector<quint64>& timeData) {
    _timeData = timeData;
}

void TimeDialog::plot() {
    const double transCoeff = 1e6;
    MultiHistogram histogram;
    histogram.addColumn("Построчный с затравкой", ((double)_timeData[0]/transCoeff), Qt::cyan);
    histogram.addColumn("По ребрам", ((double)_timeData[1]/transCoeff), Qt::red);
    histogram.addColumn("По ребрам с перегородкой", ((double)_timeData[2]/transCoeff), Qt::green);
    histogram.addColumn("Со списком ребер и флагом", ((double)_timeData[3]/transCoeff), Qt::blue);
    histogram.addColumn("С упорядоченным списком ребер,\n исп. список активных ребер", ((double)_timeData[4]/transCoeff), Qt::magenta);

    QwtLegend* legend = new QwtLegend();

    ui->plot->insertLegend(legend);
    ui->plot->setAxisTitle(QwtPlot::yLeft, QString("Время теста (мс)"));
    histogram.attach(ui->plot);

    QwtPlotGrid* grid = new QwtPlotGrid();
    grid->enableX(false);
    grid->enableYMin(true);
    grid->setMajPen(QPen(Qt::black, 0, Qt::DotLine));
    grid->setMinPen(QPen(Qt::gray, 0, Qt::DotLine));
    grid->attach(ui->plot);

   ui->plot->replot();
}

TimeDialog::~TimeDialog() {
    delete ui;
}
