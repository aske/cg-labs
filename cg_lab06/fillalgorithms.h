#ifndef FILL_DRAW_HPP
#define FILL_DRAW_HPP

#include <QImage>
#include <QColor>
#include <QLine>
#include <QList>

class step_callback {
public:
    virtual bool step() = 0;
};

struct Fill {
    QImage *image;
    QColor fillColor;
    virtual qint64 fill(step_callback *callback) = 0;
};

struct FloodfillFill : public Fill {
    QColor borderColor;
    QPoint start;
    qint64 fill(step_callback *callback);
};

struct LAEFill : public Fill {
    QList<QLine> *lines;
    qint64 fill(step_callback *callback);
};

struct EdgesFill : public Fill {
    QColor backColor;
    QList<QLine> *lines;
    qint64 fill(step_callback *callback);
};

struct BorderFill : public Fill {
    QColor backColor;
    QList<QLine> *lines;
    int border;
    qint64 fill(step_callback *callback);
};

struct FlagFill : public Fill {
private:
    void special_line(const QLine& line, const int width,
                      const QRgb& rBorder, const QRgb& rBack);
public:
    QColor backColor;
    QColor borderColor;
    QList<QLine> *lines;
    qint64 fill(step_callback *callback);
};

#endif // FILL_DRAW_HPP
