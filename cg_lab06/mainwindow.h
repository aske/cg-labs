#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>
#include <QColor>
#include <QLine>
#include <QList>
#include <QThread>
#include <QMutex>
#include <QImage>
#include <QPoint>
#include <QTimer>
#include "fillalgorithms.h"

namespace Ui {
class MainWindow;
}

class worker_thread : public QThread {
    Q_OBJECT

signals:
    void completed();

public:
    worker_thread() : fill(NULL), result(0), animation(false), stop(false) {}

    Fill *fill;
    QMutex stepWait, animationWait;
    quint64 result;
    bool animation;
    bool stop;

    void run();
};

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_canvas_mouseMoved(QMouseEvent *);
    void on_pushButton_foreColor_clicked();
    void on_pushButton_backColor_clicked();
    void on_pushButton_4_clicked();
    void on_canvas_mouseReleased(QMouseEvent *);
    void on_canvas_mousePressed(QMouseEvent *);
    void on_pushButton_5_clicked();
    void on_pushButton_clicked();
    void on_pushButton_3_clicked();
    void worker_finished();
    void animation_tick();
    void on_pushButton_6_clicked();
    void on_pushButton_speed_clicked();
    void on_pushButton_softClear_clicked();


private:
    Ui::MainWindow *ui;
    QColor foreColor, backColor, fillColor;
    QPoint oldState;
    QPainter *linePainter;
    QList<QLine> lines, *curr_lines;
    worker_thread* workerThread;
    QTimer* animationTimer;
    QImage* lineBuffer;
    bool line_drawing;
    void endLineDrawing();
    void drawLines();
    void softClear();

    void updateForeColor();
    void updateBackColor();
    void updateFillColor();
};

#endif // MAINWINDOW_HPP
