#ifndef QMOUSECANVAS_H
#define QMOUSECANVAS_H

#include <QLabel>
#include <QMouseEvent>
#include <QResizeEvent>
#include <QImage>
#include <QPainter>

class QMouseCanvas : public QWidget {
    Q_OBJECT
public:
    explicit QMouseCanvas(QWidget *parent = 0);
    ~QMouseCanvas();
    QColor fillColor() const;
    void setFillColor(const QColor &);
    QImage *image();
    void setImage(QImage *);
    void fill();
    QPainter *painter();

private:
    QImage *_image;
    QPainter *_painter, *_image_painter;
    QColor _color;
    void mousePressEvent(QMouseEvent *);
    void mouseMoveEvent(QMouseEvent *);
    void mouseReleaseEvent(QMouseEvent *);
    void mouseDoubleClickEvent(QMouseEvent *);
    void paintEvent(QPaintEvent *);
    void resizeEvent(QResizeEvent *);
    
signals:
    void mouseMoved(QMouseEvent *);
    void mousePressed(QMouseEvent *);
    void mouseReleased(QMouseEvent *);
    void mouseDoubleClicked(QMouseEvent *);
    
public slots:
    
};

#endif // QCANVAS_HPP
