#ifndef COMMON_HPP
#define COMMON_HPP

#include <qmath.h>

const float pi = 3.14159265;

template <typename T> int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}

#endif // COMMON_HPP
