#include "multihistogram.h"

MultiHistogram::MultiHistogram() {
    _size = 0;
    QwtPlotHistogram* histogram = new QwtPlotHistogram();
    QVector<QwtIntervalSample> sample(1);
    sample[0] = QwtIntervalSample(0, _size, _size+1);
    histogram->setSamples(sample);
    _histograms.push_back(histogram);
}

MultiHistogram::~MultiHistogram() {
    _histograms.clear();
}

void MultiHistogram::addColumn(const QString& title, const double value, const QColor& color) {
    QwtPlotHistogram* histogram = new QwtPlotHistogram(title);
    QVector<QwtIntervalSample> sample(1);
    sample[0] = QwtIntervalSample(value, _size, _size+1);
    histogram->setBrush(QBrush(color));
    histogram->setSamples(sample);
    _histograms.push_back(histogram);
    ++_size;
}

void MultiHistogram::attach(QwtPlot*& plot) {
    foreach(QwtPlotHistogram* histogram, _histograms)
        histogram->attach(plot);
}
