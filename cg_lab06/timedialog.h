#ifndef TIMEDIALOG_H
#define TIMEDIALOG_H

#include <QDialog>
#include <QImage>
#include <QPainter>
#include <QVector>
#include <qwt/qwt_plot.h>

namespace Ui {
class TimeDialog;
}

class TimeDialog : public QDialog {
    Q_OBJECT
    
public:
    explicit TimeDialog(QWidget *parent = 0);
    ~TimeDialog();
    void setTimeData(const QVector<quint64>& timeData);
    void plot();
    
private:
    Ui::TimeDialog *ui;
    QVector<quint64> _timeData;
};

#endif // TIMEDIALOG_H
