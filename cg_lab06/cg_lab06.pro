#-------------------------------------------------
#
# Project created by QtCreator 2012-03-29T20:28:45
#
#-------------------------------------------------

QT       += core gui xml

TARGET = cg_lab06
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    qmousecanvas.cpp \
    fillalgorithms.cpp \
    multihistogram.cpp \
    timedialog.cpp

HEADERS  += mainwindow.h \
    qmousecanvas.h \
    common.h \
    fillalgorithms.h \
    multihistogram.h \
    timedialog.h

FORMS    += mainwindow.ui \
    timedialog.ui

unix|win32: LIBS += -lqwt
